/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `_masterspa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `_masterspa`;

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `main_id` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `url` text NOT NULL,
  `status` enum('active','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `module_category_FK` (`cat_id`),
  CONSTRAINT `module_category_FK` FOREIGN KEY (`cat_id`) REFERENCES `module_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`id`, `cat_id`, `main_id`, `sort_order`, `name`, `url`, `status`) VALUES
	(1, 1, 0, 0, 'Account List', '\\account\\manage\\list', 'active'),
	(2, 2, 0, 1, 'Asset List', '\\asset\\manage\\list', 'active'),
	(3, 3, 0, 2, 'Spas Management', '\\spas\\manage\\list', 'active');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `module_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `status` enum('active','deleted') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

/*!40000 ALTER TABLE `module_category` DISABLE KEYS */;
INSERT INTO `module_category` (`id`, `name`, `status`) VALUES
	(1, 'Account', 'active'),
	(2, 'Asset', 'active'),
	(3, 'Features', 'active');
/*!40000 ALTER TABLE `module_category` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `serial_no` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('active','cancelled','deleted') NOT NULL,
  `order_status` enum('in_warehouse','in_transit') NOT NULL DEFAULT 'in_warehouse',
  `steps` int(11) DEFAULT NULL,
  `covers` int(11) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_orders_product` (`serial_no`),
  KEY `FK_orders_user` (`user_id`),
  CONSTRAINT `FK_orders_product` FOREIGN KEY (`serial_no`) REFERENCES `product` (`serial_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_orders_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf32;

/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `serial_no`, `user_id`, `status`, `order_status`, `steps`, `covers`, `created_at`, `updated_at`) VALUES
	(000003, '1707255', 2, 'active', 'in_warehouse', NULL, NULL, NULL, NULL),
	(000004, '1712853', 2, 'active', 'in_warehouse', 12, 9, '2017/12/22 10:00:25', '2017/12/22 10:00:25'),
	(000005, '1712873', 2, 'active', 'in_warehouse', 12, 9, '2017/12/22 10:07:14', '2017/12/22 10:07:14');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product` (
  `serial_no` varchar(20) NOT NULL,
  `model` varchar(255) NOT NULL,
  `color` varchar(100) NOT NULL,
  `skirt` varchar(100) NOT NULL,
  `leds` varchar(50) DEFAULT NULL,
  `full_foam` varchar(50) DEFAULT NULL,
  `stereo` varchar(20) DEFAULT NULL,
  `special` varchar(20) DEFAULT NULL,
  `jets` varchar(20) DEFAULT NULL,
  `frame` varchar(20) DEFAULT NULL,
  `ext` varchar(20) DEFAULT NULL,
  `order_sage_no` varchar(20) DEFAULT NULL,
  `date_completed` varchar(50) NOT NULL,
  `option_str` varchar(20) DEFAULT NULL,
  `revision` varchar(20) DEFAULT NULL,
  `status` enum('available','processing','sold') NOT NULL DEFAULT 'available',
  `ignore_flag` int(11) NOT NULL DEFAULT '0',
  `type` int(11) DEFAULT '1',
  PRIMARY KEY (`serial_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`serial_no`, `model`, `color`, `skirt`, `leds`, `full_foam`, `stereo`, `special`, `jets`, `frame`, `ext`, `order_sage_no`, `date_completed`, `option_str`, `revision`, `status`, `ignore_flag`, `type`) VALUES
	('1706718', 'INT MPL 700', 'Sterling Silver Marble', 'Black', '', '', 'FT', '', 'LED', '', '', NULL, '2017-05-11', '2423  04    10    ', 'WEE', 'available', 0, 1),
	('1706876', 'INT TS 87.3', 'Sterling Silver Marble', 'Portabello PolyPremium', '', '', '', 'PUR', 'SSJ', 'ABS', '', NULL, '2017-05-10', '2411  02  02  03  ', 'WEE', 'available', 0, 1),
	('1707172', 'INT TS 7.2', 'Sterling Silver Marble', 'Portabello PolyPremium', '', '', '', 'PUR', 'SSJ', 'ABS', '', NULL, '2017-05-15', '2411  02  02  03  ', 'WEE', 'available', 0, 1),
	('1707181', 'INT EVLTN 7.1', 'Sterling Silver Marble', 'Portabello PolyPremium', 'X', 'X', '', '', 'SSJ', '', '', NULL, '2017-05-17', '2411020201        ', 'WEE', 'available', 0, 1),
	('1707182', 'INT EVLTN 7.1', 'Sterling Silver Marble', 'Espresso PolyPremium', 'X', 'X', '', '', 'SSJ', '', '', NULL, '2017-05-17', '2419020201        ', 'WEE', 'available', 0, 1),
	('1707187', 'INT EVLTN 7.3', 'Sterling Silver Marble', 'Portabello PolyPremium', 'X', 'X', '', 'PUR', 'SSJ', '', '', NULL, '2017-05-17', '241102020102      ', 'WEE', 'available', 0, 1),
	('1707188', 'INT EVLTN 7.3', 'Sterling Silver Marble', 'Portabello PolyPremium', 'X', 'X', '', 'PUR', 'SSJ', '', '', NULL, '2017-05-16', '241102020102      ', 'WEE', 'available', 0, 1),
	('1707255', 'INT EVLTN 5', 'Sterling Silver Marble', 'Espresso PolyPremium', '', 'X', '', '', 'SSJ', '', '', NULL, '2017-05-18', '24190202          ', 'WEE', 'available', 0, 1),
	('1707267', 'INT MPL 800', 'Sterling Silver Marble', 'Espresso PolyPremium', '', '', 'FT', '', 'LED', '', '', NULL, '2017-05-19', '2419  04    10    ', 'WEE', 'available', 0, 1),
	('1707280', 'INT TS 6.2', 'Sterling Silver Marble', 'Portabello PolyPremium', '', '', '', 'PUR', 'SSJ', '', '', NULL, '2017-05-18', '2411  02  02      ', 'WEE', 'available', 0, 1),
	('1707552', 'INT EVLTN 5', 'Sterling Silver Marble', 'Portabello PolyPremium', '', 'X', '', '', 'SSJ', '', '', NULL, '2017-05-24', '24110202          ', 'WEE', 'available', 0, 1),
	('1707888', 'INT EVLTN 7.3', 'Sterling Silver Marble', 'Espresso PolyPremium', 'X', 'X', '', 'PUR', 'SSJ', '', '', NULL, '2017-05-31', '241902020102      ', 'WEE', 'available', 0, 1),
	('1712853', 'INT EVLTN 7.3', 'Sterling Silver Marble', '', 'X', 'X', '', 'PUR', 'SSJ', '', '', NULL, '2017-09-19', '241102020102      ', 'WEE', 'sold', 0, 1),
	('1712873', 'INT TS 7.2', 'Sterling Silver Marble', '', '', '', 'Wi-Fi', 'PUR', 'SSJ', 'ABS', '', NULL, '2017-09-19', '2420  02  020403  ', 'WEE', 'sold', 0, 1),
	('1712896', 'INT TS 8.3', 'Sterling Silver Marble', 'Driftwood (MasterTech)', '', '', 'Wi-Fi', 'PUR', 'SSJ', 'ABS', '', NULL, '2017-09-13', '2422  02  020403  ', 'WEE', 'available', 0, 1),
	('H170763', 'INT H2X TRAIN19', 'Sterling Silver Marble', 'Espresso PolyPremium', '', '', '', 'PUR', 'SSJ', 'ST', '', NULL, '2017-05-19', '2419  03  02  04  ', 'WEE', 'available', 0, 1),
	('H170778', 'INT H2X TRAIN15', 'Sterling Silver Marble', 'Espresso PolyPremium', '', '', '', 'PUR', 'SSJ', 'ST', '', NULL, '2017-05-23', '2419  02  02  04  ', 'WEE', 'available', 0, 1),
	('H171605', 'INT MP MOMEN D', 'Sterling Silver Marble', 'Driftwood (MasterTech)', '', 'XP', '', 'PUR', 'SSJ', '', '', NULL, '2017-09-12', '24220403  02  01  ', 'WEE', 'available', 0, 1),
	('R170554', 'INT GS OCHO R L', 'Sea Salt', 'Espresso', '', 'X', '', '', '', '', '', NULL, '2017-06-19', '51                ', 'WEE', 'available', 0, 1),
	('R170555', 'INT GS OCHO R L', 'Sea Salt', 'Espresso', '', 'X', '', '', '', '', '', NULL, '2017-06-08', '51                ', 'WEE', 'available', 0, 1),
	('R170884', 'INT GS BARHAR L', 'Sea Salt', 'Espresso', '', 'X', '', '', '', '', '', NULL, '2017-09-07', '51                ', 'WEE', 'available', 0, 1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `steps_covers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` enum('steps','covers') NOT NULL DEFAULT 'steps',
  `q_type` enum('swim_spa','hot_tub') NOT NULL DEFAULT 'swim_spa',
  `status` enum('active','deleted') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf32;

/*!40000 ALTER TABLE `steps_covers` DISABLE KEYS */;
INSERT INTO `steps_covers` (`id`, `name`, `description`, `type`, `q_type`, `status`) VALUES
	(1, 'Test1', 'desc1', 'steps', 'swim_spa', 'active'),
	(2, 'test2', 'desc2', 'covers', 'swim_spa', 'active'),
	(3, 'Test3', '', 'steps', 'hot_tub', 'active'),
	(4, 'test4', 'desc4', 'steps', 'hot_tub', 'active'),
	(5, 'Steps Swim Spa 3', 'desc5', 'covers', 'swim_spa', 'active'),
	(6, 'Test6', 'desc6', 'covers', 'hot_tub', 'active'),
	(7, 'Test', 'TEstwqeqwewqewqWEwaeqwe', 'steps', 'hot_tub', 'active'),
	(8, 'Test1213123', '4123123', 'covers', 'hot_tub', 'active'),
	(9, 'AAAA', 'AAAA', 'covers', 'hot_tub', 'active'),
	(10, 'BBBB', 'BBBB', 'covers', 'hot_tub', 'active'),
	(11, 'AAA1', 'AAA1', 'steps', 'swim_spa', 'active'),
	(12, 'AAA2', 'AAA2', 'steps', 'hot_tub', 'active'),
	(13, 'AAA3', 'AAA3', 'steps', 'hot_tub', 'active');
/*!40000 ALTER TABLE `steps_covers` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` enum('admin','user') NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `customer_no` varchar(100) NOT NULL,
  `dealer` varchar(100) NOT NULL,
  `status` enum('active','inactive','unconfirmed') NOT NULL,
  `ins_time` datetime NOT NULL,
  `up_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf32;

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `user_type`, `username`, `password`, `email_address`, `customer_no`, `dealer`, `status`, `ins_time`, `up_time`) VALUES
	(1, 'admin', 'admin', '$2y$13$Na13dP3go1ZwGwV8Be5X3e03CLeG1.FZiAbEue8wSdwirpc9429j2', 'test@gmail.com', '', '', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'user', 'test1', '$2y$13$76aDYdleRIJH/0sGCIpP/.OWpCEDXiAMJoCm3jDDQ3jxtFxIMnt7O', 'test1@gmail.com', 'TEST1', 'test1', 'active', '2017-10-18 23:00:00', '2017-10-18 23:00:00');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` text,
  `contact_number` varchar(50) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_userInfo` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf32;

/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` (`id`, `user_id`, `first_name`, `middle_name`, `last_name`, `address`, `contact_number`, `status`) VALUES
	(1, 1, 'Dash', 'Codes', 'Ph', 'Cebu City', '0129312', 'active'),
	(2, 2, 'test1', NULL, 'test1', NULL, NULL, 'active');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
