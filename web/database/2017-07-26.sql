-- --------------------------------------------------------
-- Host:                         192.168.254.112
-- Server version:               5.6.29 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for _commondb
DROP DATABASE IF EXISTS `_commondb`;
CREATE DATABASE IF NOT EXISTS `_commondb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `_commondb`;

-- Dumping structure for table _commondb.asset
DROP TABLE IF EXISTS `asset`;
CREATE TABLE IF NOT EXISTS `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `media_type` enum('photo','video','zip') NOT NULL,
  `file_url` varchar(255) DEFAULT NULL,
  `tiny_url` varchar(255) DEFAULT NULL,
  `small_url` varchar(255) DEFAULT NULL,
  `medium_url` varchar(255) DEFAULT NULL,
  `large_url` varchar(255) DEFAULT NULL,
  `file_size` float DEFAULT NULL,
  `ins_time` datetime NOT NULL,
  `up_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf32;

-- Dumping data for table _commondb.asset: ~2 rows (approximately)
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` (`id`, `user_id`, `media_type`, `file_url`, `tiny_url`, `small_url`, `medium_url`, `large_url`, `file_size`, `ins_time`, `up_time`) VALUES
	(6, 1, 'photo', 'http://iwia.dev/resources/uploads/photos/1/6/original.png', 'http://iwia.dev/resources/uploads/photos/1/6/tiny.png', 'http://iwia.dev/resources/uploads/photos/1/6/small.png', 'http://iwia.dev/resources/uploads/photos/1/6/medium.png', 'http://iwia.dev/resources/uploads/photos/1/6/large.png', 16921, '2017-06-24 00:15:30', '2017-06-24 00:15:30'),
	(7, 1, 'photo', 'http://iwia.dev/resources/uploads/photos/1/7/original.png', 'http://iwia.dev/resources/uploads/photos/1/7/tiny.png', 'http://iwia.dev/resources/uploads/photos/1/7/small.png', 'http://iwia.dev/resources/uploads/photos/1/7/medium.png', 'http://iwia.dev/resources/uploads/photos/1/7/large.png', 13873, '2017-06-28 22:11:39', '2017-06-28 22:11:39');
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;

-- Dumping structure for table _commondb.modules
DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `main_id` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `url` text NOT NULL,
  `status` enum('active','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `module_category_FK` (`cat_id`),
  CONSTRAINT `module_category_FK` FOREIGN KEY (`cat_id`) REFERENCES `module_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

-- Dumping data for table _commondb.modules: ~3 rows (approximately)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`id`, `cat_id`, `main_id`, `sort_order`, `name`, `url`, `status`) VALUES
	(1, 1, 0, 0, 'Account List', '\\account\\manage\\list', 'active'),
	(2, 2, 0, 1, 'Asset List', '\\asset\\manage\\list', 'active'),
	(3, 3, 0, 2, 'Product Management', '\\product\\manage\\list', 'active');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

-- Dumping structure for table _commondb.module_category
DROP TABLE IF EXISTS `module_category`;
CREATE TABLE IF NOT EXISTS `module_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `status` enum('active','deleted') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

-- Dumping data for table _commondb.module_category: ~3 rows (approximately)
/*!40000 ALTER TABLE `module_category` DISABLE KEYS */;
INSERT INTO `module_category` (`id`, `name`, `status`) VALUES
	(1, 'Account', 'active'),
	(2, 'Asset', 'active'),
	(3, 'Features', 'active');
/*!40000 ALTER TABLE `module_category` ENABLE KEYS */;

-- Dumping structure for table _commondb.order
DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_no` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('active','deleted','cancelled','delivered') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Dumping data for table _commondb.order: ~0 rows (approximately)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` (`id`, `serial_no`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
	(19, 'R170499', 2, 'active', '2017-07-26 22:53:17', '2017-07-26 22:53:17');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Dumping structure for table _commondb.product
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `serial_no` varchar(20) NOT NULL,
  `model` varchar(255) NOT NULL,
  `color` varchar(100) NOT NULL,
  `skirt` varchar(100) NOT NULL,
  `leds` varchar(50) DEFAULT NULL,
  `full_foam` varchar(50) DEFAULT NULL,
  `stereo` varchar(20) DEFAULT NULL,
  `special` varchar(20) DEFAULT NULL,
  `frame` varchar(20) DEFAULT NULL,
  `jets` varchar(20) DEFAULT NULL,
  `ext` varchar(20) DEFAULT NULL,
  `date_completed` date NOT NULL,
  `status` enum('available','processing','sold') NOT NULL,
  PRIMARY KEY (`serial_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- Dumping data for table _commondb.product: ~15 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`serial_no`, `model`, `color`, `skirt`, `leds`, `full_foam`, `stereo`, `special`, `frame`, `jets`, `ext`, `date_completed`, `status`) VALUES
	('H170396', 'H2X TRAINER12', 'Sterling Silver Marble', 'Expresso PolyPremium', 'X', '', 'BTA', '', 'ST', '', '', '2017-03-16', ''),
	('H170629', 'H2X THERAPOSE', 'Sierra', 'Expresso PolyPremium', 'X', '', '', '', 'ST', 'SSJ', '', '2017-05-01', ''),
	('H170673', 'H2X THERAPOSE', 'Sierra', 'Expresso PolyPremium', 'X', '', '', '', 'ST', 'SSJ', '', '2017-05-09', ''),
	('R170401', 'GS BARHARBOR LE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-10', ''),
	('R170463', 'GS BARHARBOR SE', 'Sea Salt', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-19', ''),
	('R170464', 'GS BARHARBOR SE', 'Sea Salt', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-22', ''),
	('R170466', 'GS BARHARBOR SE', 'Sea Salt', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-19', ''),
	('R170473', 'GS BARHARBOR SE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-23', ''),
	('R170474', 'GS BARHARBOR SE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-23', ''),
	('R170480', 'GS BARHARBOR SE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-13', ''),
	('R170498', 'GS BARHARBOR LE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-25', ''),
	('R170499', 'GS BARHARBOR LE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-15', 'processing'),
	('R170520', 'GS BARHARBOR LE', 'Teakwood', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-07', ''),
	('R170521', 'GS BARHARBOR LE', 'Teakwood', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-06', ''),
	('R170524', 'GS BARHARBOR LE', 'Teakwood', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-08', '');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table _commondb.project
DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `url` varchar(255) NOT NULL,
  `ins_time` datetime NOT NULL,
  `up_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_projectAsset` (`asset_id`),
  KEY `FK_projectUser` (`user_id`),
  CONSTRAINT `FK_projectAsset` FOREIGN KEY (`asset_id`) REFERENCES `asset` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_projectUser` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf32;

-- Dumping data for table _commondb.project: ~2 rows (approximately)
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` (`id`, `user_id`, `asset_id`, `name`, `description`, `url`, `ins_time`, `up_time`) VALUES
	(5, 1, 6, 'sdfds', '', 'fdsfds', '2017-06-24 00:15:30', '2017-06-24 00:15:30'),
	(6, 1, 7, 'testproject', 'sdfsd', 'testproject', '2017-06-28 22:11:39', '2017-06-28 22:11:39');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;

-- Dumping structure for table _commondb.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` enum('admin','user') NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `ins_time` datetime NOT NULL,
  `up_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf32;

-- Dumping data for table _commondb.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `user_type`, `username`, `password`, `email_address`, `status`, `ins_time`, `up_time`) VALUES
	(1, 'admin', 'admin', '$2y$13$Na13dP3go1ZwGwV8Be5X3e03CLeG1.FZiAbEue8wSdwirpc9429j2', 'dashcodesph@gmail.com', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'user', 'user1', '$2y$13$Na13dP3go1ZwGwV8Be5X3e03CLeG1.FZiAbEue8wSdwirpc9429j2', 'user1@gmail.com', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table _commondb.user_info
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` text,
  `contact_number` varchar(50) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_userInfo` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32;

-- Dumping data for table _commondb.user_info: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` (`id`, `user_id`, `first_name`, `middle_name`, `last_name`, `address`, `contact_number`, `status`) VALUES
	(1, 1, 'Dash', 'Codes', 'Ph', 'Cebu City', '09233653362', 'active');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
