-- --------------------------------------------------------
-- Host:                         192.168.254.110
-- Server version:               5.6.29 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for _masterspa
DROP DATABASE IF EXISTS `_masterspa`;
CREATE DATABASE IF NOT EXISTS `_masterspa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `_masterspa`;

-- Dumping structure for table _masterspa.modules
DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `main_id` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `url` text NOT NULL,
  `status` enum('active','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `module_category_FK` (`cat_id`),
  CONSTRAINT `module_category_FK` FOREIGN KEY (`cat_id`) REFERENCES `module_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

-- Dumping data for table _masterspa.modules: ~3 rows (approximately)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`id`, `cat_id`, `main_id`, `sort_order`, `name`, `url`, `status`) VALUES
	(1, 1, 0, 0, 'Account List', '\\account\\manage\\list', 'active'),
	(2, 2, 0, 1, 'Asset List', '\\asset\\manage\\list', 'active'),
	(3, 3, 0, 2, 'Spas Management', '\\spas\\manage\\list', 'active');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

-- Dumping structure for table _masterspa.module_category
DROP TABLE IF EXISTS `module_category`;
CREATE TABLE IF NOT EXISTS `module_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `status` enum('active','deleted') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

-- Dumping data for table _masterspa.module_category: ~3 rows (approximately)
/*!40000 ALTER TABLE `module_category` DISABLE KEYS */;
INSERT INTO `module_category` (`id`, `name`, `status`) VALUES
	(1, 'Account', 'active'),
	(2, 'Asset', 'active'),
	(3, 'Features', 'active');
/*!40000 ALTER TABLE `module_category` ENABLE KEYS */;

-- Dumping structure for table _masterspa.orders
DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_no` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('active','cancelled','deleted') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_orders_product` (`serial_no`),
  KEY `FK_orders_user` (`user_id`),
  CONSTRAINT `FK_orders_product` FOREIGN KEY (`serial_no`) REFERENCES `product` (`serial_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_orders_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf32;

-- Dumping data for table _masterspa.orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `serial_no`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
	(22, 'R170499', 2, 'active', '2017-07-29 21:43:24', '2017-07-29 21:43:24'),
	(23, 'R170480', 2, 'active', '2017-07-29 21:43:30', '2017-07-29 21:43:30'),
	(24, 'R170480', 2, 'cancelled', '2017-07-29 22:06:45', '2017-07-29 22:06:45'),
	(25, 'R170524', 3, 'cancelled', '2017-07-29 22:07:05', '2017-07-29 22:07:05');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table _masterspa.product
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `serial_no` varchar(20) NOT NULL,
  `model` varchar(255) NOT NULL,
  `color` varchar(100) NOT NULL,
  `skirt` varchar(100) NOT NULL,
  `leds` varchar(50) DEFAULT NULL,
  `full_foam` varchar(50) DEFAULT NULL,
  `stereo` varchar(20) DEFAULT NULL,
  `special` varchar(20) DEFAULT NULL,
  `frame` varchar(20) DEFAULT NULL,
  `jets` varchar(20) DEFAULT NULL,
  `ext` varchar(20) DEFAULT NULL,
  `date_completed` date NOT NULL,
  `status` enum('available','processing','sold') NOT NULL DEFAULT 'available',
  PRIMARY KEY (`serial_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- Dumping data for table _masterspa.product: ~0 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`serial_no`, `model`, `color`, `skirt`, `leds`, `full_foam`, `stereo`, `special`, `frame`, `jets`, `ext`, `date_completed`, `status`) VALUES
	('R170401', 'GS BARHARBOR LE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-10', 'available'),
	('R170473', 'GS BARHARBOR SE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-23', 'available'),
	('R170474', 'GS BARHARBOR SE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-23', 'available'),
	('R170480', 'GS BARHARBOR SE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-13', 'available'),
	('R170498', 'GS BARHARBOR LE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-05-25', 'available'),
	('R170499', 'GS BARHARBOR LE', 'Sand Dune', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-15', 'available'),
	('R170520', 'GS BARHARBOR LE', 'Teakwood', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-07', 'available'),
	('R170521', 'GS BARHARBOR LE', 'Teakwood', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-06', 'available'),
	('R170524', 'GS BARHARBOR LE', 'Teakwood', 'Expresso PolyPremium', '', 'X', '', '', '', '', '', '2017-06-08', 'available');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table _masterspa.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` enum('admin','user') NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `ins_time` datetime NOT NULL,
  `up_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

-- Dumping data for table _masterspa.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `user_type`, `username`, `password`, `email_address`, `status`, `ins_time`, `up_time`) VALUES
	(1, 'admin', 'admin', '$2y$13$Na13dP3go1ZwGwV8Be5X3e03CLeG1.FZiAbEue8wSdwirpc9429j2', 'dashcodesph@gmail.com', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'user', 'user1', '$2y$13$Na13dP3go1ZwGwV8Be5X3e03CLeG1.FZiAbEue8wSdwirpc9429j2', 'user1@gmail.com', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 'user', 'user2', '$2y$13$Na13dP3go1ZwGwV8Be5X3e03CLeG1.FZiAbEue8wSdwirpc9429j2', 'user1@gmail.com', 'active', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table _masterspa.user_info
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` text,
  `contact_number` varchar(50) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_userInfo` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

-- Dumping data for table _masterspa.user_info: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` (`id`, `user_id`, `first_name`, `middle_name`, `last_name`, `address`, `contact_number`, `status`) VALUES
	(1, 1, 'Dash', 'Codes', 'Ph', 'Cebu City', '09233653362', 'active'),
	(2, 2, 'User', '-', 'One', 'Cebu City', '09456217890', 'active'),
	(3, 3, 'User', '-', 'Two', 'Cebu City', '09456217890', 'active');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
