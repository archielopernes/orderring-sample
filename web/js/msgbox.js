$('.cancel-btn').on('click', function() {
    var id = $(this).attr('id');
    console.log(id);

    swal({
      title: "Cancel Order",
      text: "Do you want to cancel order?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
             $.ajax({
                type: 'POST',
                url: '/order/manage/cancel/' + id,
                success: function (url) {
                    console.log(url);
                    if (url !== false) {
                         setTimeout(function() {
                            swal({
                                title: "Cancel Order!",
                                text: "Order cancelled",
                                type: "success",
                                confirmButtonText: "Ok"
                            }, function() {
                                window.location = "/order/manage";
                            }, 1000);
                        });

                    } else {
                        swal("Cancel Order", "Order not cancelled, please try again later.", "error");
                    }
                },
            });

        }
    });
});

$('.delete-account-btn').on('click', function() {
    var id = $(this).attr('id');

    swal({
      title: "Delete Account",
      text: "Do you want to delete account?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
             $.ajax({
                type: 'POST',
                url: '/account/manage/delete/' + id,
                success: function (url) {
                    console.log(url);
                    if (url !== false) {
                         setTimeout(function() {
                            swal({
                                title: "Delete Account",
                                text: "Account deleted",
                                type: "success",
                                confirmButtonText: "Ok"
                            }, function() {
                                window.location = "/account/manage";
                            }, 1000);
                        });

                    } else {
                        swal("Delete Account", "Account not deleted, please try again later", "error");
                    }
                },
            });

        }
    });
});

$('.activate-account-btn').on('click', function() {
    var id = $(this).attr('id');

    swal({
      title: "Activate Account",
      text: "Do you want to activate this account?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
             $.ajax({
                type: 'POST',
                url: '/account/manage/activate/' + id,
                success: function (url) {
                    console.log(url);
                    if (url !== false) {
                         setTimeout(function() {
                            swal({
                                title: "Activate Account",
                                text: "Account activated",
                                type: "success",
                                confirmButtonText: "Ok"
                            }, function() {
                                window.location = "/account/manage";
                            }, 1000);
                        });

                    } else {
                        swal("Delete Account", "Account not deleted, please try again later", "error");
                    }
                },
            });

        }
    });
});

$('.warehouse-btn').on('click', function() {
    var id = $(this).attr('id');

    swal({
      title: "Change Order Status",
      text: "Do you want to change order status to 'In Transit'?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
             $.ajax({
                type: 'POST',
                url: '/order/manage/changetransit/' + id,
                success: function (url) {
                    console.log(url);
                    if (url !== false) {
                         setTimeout(function() {
                            swal({
                                title: "Change Order Status",
                                text: "Successfully Change Order Status",
                                type: "success",
                                confirmButtonText: "Ok"
                            }, function() {
                                window.location = "/order/manage";
                            }, 1000);
                        });

                    }
                },
            });

        }
    });
});

$('.transit-btn').on('click', function() {
    var id = $(this).attr('id');

    swal({
      title: "Change Order Status",
      text: "Do you want to change order status to 'In Warehouse'?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
             $.ajax({
                type: 'POST',
                url: '/order/manage/changewarehouse/' + id,
                success: function (url) {
                    console.log(url);
                    if (url !== false) {
                         setTimeout(function() {
                            swal({
                                title: "Change Order Status",
                                text: "Successfully Change Order Status",
                                type: "success",
                                confirmButtonText: "Ok"
                            }, function() {
                                window.location = "/order/manage";
                            }, 1000);
                        });

                    }
                },
            });

        }
    });
});

$('.delete-steps-covers-btn').on('click', function() {
    var id = $(this).attr('id');

    swal({
      title: "Delete Step / Order",
      text: "Are you sure you want to delete step or order?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
             $.ajax({
                type: 'POST',
                url: '/stepscovers/manage/delete/' + id,
                success: function (url) {
                    console.log(url);
                    if (url !== false) {
                         setTimeout(function() {
                            swal({
                                title: "Delete Step / Order",
                                text: "Successfully Deleted step/cover",
                                type: "success",
                                confirmButtonText: "Ok"
                            }, function() {
                                window.location = "/stepscovers/manage";
                            }, 1000);
                        });

                    }
                },
            });

        }
    });
});

$('.ignore-flag').on('click', function() {
    var value = $(this).val();
    var serial_no = $(this).attr('id');
    var title = 'Ignore on Import';
    var msg = 'Are you sure you want to ignore record on import?';
    if(value == 0) {
        title = 'Remove ignore on Import';
        msg = 'Are you sure you want to remove ignore record on import?';
    }

     swal({
      title: title,
      text: msg,
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
             $.ajax({
                type: 'POST',
                url: '/spas/manage/flag/' + serial_no + '/' + value,
                success: function (url) {
                    console.log(url);
                    if (url !== false) {
                         setTimeout(function() {
                            swal({
                                title: title + ' successful',
                                type: "success",
                                confirmButtonText: "Ok"
                            }, function() {
                                window.location = "/spas/manage";
                            }, 1000);
                        });

                    }
                },
            });

        }
    });

});

$('.delete-spa').on('click', function() {
    var id = $(this).attr('id');

     swal({
      title: 'Delete Spa',
      text: 'Are you sure you want to delete Spa? Order data will also be deleted.',
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
             $.ajax({
                type: 'POST',
                url: '/spas/manage/delete/' + id,
                success: function (url) {
                    console.log(url);
                    if (url !== false) {
                         setTimeout(function() {
                            swal({
                                title: 'Spa Successfully Deleted',
                                type: "success",
                                confirmButtonText: "Ok"
                            }, function() {
                                window.location = "/spas/manage";
                            }, 1000);
                        });

                    }
                },
            });

        }
     });
});

$('.delete-order').on('click', function() {
    var id = $(this).attr('id');

     swal({
      title: 'Delete Order',
      text: 'Are you sure you want to delete Order?',
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
             $.ajax({
                type: 'POST',
                url: '/order/manage/delete/' + id,
                success: function (url) {
                    console.log(url);
                    if (url !== false) {
                         setTimeout(function() {
                            swal({
                                title: 'Order Successfully Deleted',
                                type: "success",
                                confirmButtonText: "Ok"
                            }, function() {
                                window.location = "/order/manage";
                            }, 1000);
                        });

                    }
                },
            });

        }
     });
});

