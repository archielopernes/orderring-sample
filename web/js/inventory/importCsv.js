var csvFile = null; // Holds the CSV File

$('#csv-btn').click(function() {
    $('.csv-file').click();
});

$('.csv-file').on('change', function(e) {
    csvFile = e.target.files;
    $('#csvFileName').text(csvFile[0].name);
});

$('#importCsvModal').on('show.bs.modal', function (e) { // Hide Loader
    $(this).find('.loader').hide();
});

$('#importCsvModal').on('hide.bs.modal', function (e) { // Prevent from closing modal if loader is shown
    if($(this).find('.loader').is(":visible")) {
        e.preventDefault();
    }
});

$('#importCsvModal').on('hidden.bs.modal', function (e) { // Prevent from closing modal if loader is shown
    $('#csvFileName').text('');
    $('.field-product-csvfile').removeClass('has-error');
    $('.field-product-csvfile .help-block').text('');
});

$('#csv-submit').on('click', function(e) {
    if($('#product-csvfile').val().length == 0) {
        $('.field-product-csvfile').addClass('has-error');
        $('.field-product-csvfile .help-block').text('No CSV file added');
    }

    if(!$('.field-product-csvfile').hasClass('has-error')) {
       $(this).parents('.modal').find('.loader').show();
       $('#csv-form').submit();
       //$('#csv-form').hide();
    }



});
