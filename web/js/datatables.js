$(window).on('load', function() {
    if($('#inventory-list').length != 0) {
        var columns = [ 4,5,6,7,8,9,10,14 ];
        if ($(this).find('th').length == 15) {
            columns.push(14);
        }

        $('#inventory-list').DataTable({
            "order": [[ 11, "desc" ]],
            "pageLength": 50,
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": columns },
            ]
        });
    }

    if($('#order-list').length != 0) {
        var columns = [ ];
        if ($(this).find('th').length == 6) {
            columns.push(5);
        }
        $('#order-list').DataTable({
            "order": [[ 3, "desc" ]],
            "pageLength": 50,
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": columns },
            ]
        });
    }

    if($('#account-list').length != 0) {
        $('#account-list').DataTable({
            "order": [[ 3, "desc" ]],
            "pageLength": 50,
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [4] },
            ]
        });
    }

    if($('#steps-covers-list').length != 0) {
        $('#steps-covers-list').DataTable({
            "order": [[ 0, "asc" ]],
            "pageLength": 50,
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [5] },
            ]
        });
    }
} );
