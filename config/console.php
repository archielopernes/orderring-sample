<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
//$db = require(__DIR__ . '/db.php');

// Database Configurationss
$databases = [
    'db'  => '_masterspa',
];

$dbConfig = [
    'class' => 'yii\db\Connection',
    'dsn' => '',
    'username' => 'root',
    'password' => 'dimensions',
    'charset' => 'utf8',
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 3600,
    'schemaCache' => 'cache',
    'attributes' => [
        PDO::MYSQL_ATTR_LOCAL_INFILE => true
    ]
];

foreach ($databases as $dbKey => $dbName) {
    $dsn = 'mysql:host=localhost;dbname=' . $dbName;
    //$dsn = 'mysql:host=impact-db.cemd87iku6qt.ap-northeast-1.rds.amazonaws.com;dbname=' . $dbName;
    $dbConfig['dsn'] = $dsn;
    $$dbKey = $dbConfig;
}

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'timeZone' => 'Asia/Manila',
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db'         => $db, // Default
    ],
    'params' => $params,
];
