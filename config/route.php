<?php

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => true,
    'rules' => [
        /* Automatic referencing for urls <module>/controller/action format */
        ''
        => '/account/session/login',
        'terms-conditions'
        => '/account/session/termsconditions',
//        '/register'
//        => '/account/session/register',
        '/forgotpassword'
        => '/account/session/forgotpassword',
        '/<action:(login|logout|dashboard)>'
        => '/account/session/<action>',
        '/<module:account>/<controller:manage>'
        => '/<module>/<controller>/index',
        '/<module:account>/<controller:manage>/<action:register>'
        => '/<module>/<controller>/<action>',
        '/<module:account>/<controller:manage>/<action:(edit|delete|activate|cpass)>/<id:\d+>'
        => '/<module>/<controller>/<action>',
        /* User Information */
        '/<action:(profile|changepassword)>'
        => '/account/manage/<action>',
        /* Inventory */
        '/<module:spas>/<controller:manage>'
        => '/<module>/<controller>/index',
        '/<module:spas>/<controller:manage>/<action:importcsv>'
        => '/<module>/<controller>/<action>',
        '/<module:spas>/<controller:manage>/<action:flag>/<id:.+>/<value:\d+>'
        => '/<module>/<controller>/<action>',
        '/<module:spas>/<controller:manage>/<action:delete>/<id:.+>'
        => '/<module>/<controller>/<action>',
        '/<module:spas>/<controller:manage>/<action:spaexport>'
        => '/<module>/<controller>/<action>',
        /* Order */
        '/<module:order>/<controller:manage>/<action:orderexport>'
        => '/<module>/<controller>/<action>',
        '/<module:order>/<controller:manage>'
        => '/<module>/<controller>/list',
        '/<module:order>/<controller:manage>/<action:(cancel|changetransit|changewarehouse|delete)>/<orderId:\d+>'
        => '/<module>/<controller>/<action>',
        '/<module:order>/<controller:manage>/<action:(confirm|failed)>'
        => '/<module>/<controller>/<action>',
        '/<module:order>/<controller:manage>/<serialNo:.+>'
        => '/<module>/<controller>/index',

         /* StepsCovers */
        '/<module:stepscovers>/<controller:manage>'
        => '/<module>/<controller>/list',
        '/<module:stepscovers>/<controller:manage>/<action:(delete|edit)>/<id:\d+>'
        => '/<module>/<controller>/<action>',
        '/<module:stepscovers>/<controller:manage>/<action:add>'
        => '/<module>/<controller>/<action>',

    ]
];
?>
