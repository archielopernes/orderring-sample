<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\BaseUrl;
use yii\helpers\Url;
use app\components\helpers\Data;
use yii\base\Exception;

use app\models\Order;

class ExportorderController extends Controller {

    public function actionIndex()
    {
        $dateToday = date('Ymd');
        $file = Yii::$app->basePath.'/web/csvexport/order_csv'.$dateToday.'.csv';

        $orderModel = new Order;
        $items = $orderModel->getOrder(true);
		$output = fopen($file, 'w');
		$columnnames = array('Serial Number', 'Customer Number', 'Cover / Steps', 'Order #', 'Order Status', 'Order Date Time', 'Option String', 'Revision', 'Order');

        $arr = ['model', 'cover', 'step'];
        $temp = [];
		fputcsv($output, $columnnames);
		foreach ($items as $fields) {
            $temp = [
                'model' => $fields['model'],
                'cover' => $fields['cover'],
                'step'  => $fields['step']
            ];
            if ($fields['status'] == 'cancelled') {
                $fields['order_status'] = 'Order Cancelled';
            } else {
                if ($fields['order_status'] == 'in_warehouse') {
                    $fields['order_status'] = 'In Warehouse';
                } else {
                    $fields['order_status'] = 'In Transit';
                }
            }

            unset($fields['step'], $fields['model']);
            foreach($arr as $val) {
                if($val != 'model') {
                    $fields['serial_no'] = null;
                    $fields['option_str'] = null;
                    $fields['revision'] = null;
                }

                $fields['cover'] = $temp[$val];
                unset($fields['user_id'], $fields['steps'], $fields['covers'], $fields['updated_at'], $fields['status'], $fields['full_name']);
                fputcsv($output, $fields);
            }
		}
		fclose($output);
    }
}


?>
