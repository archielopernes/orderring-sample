<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\BaseUrl;
use yii\helpers\Url;
use app\components\helpers\Data;
use yii\base\Exception;

use app\models\Product;

class CsvController extends Controller {

    public function actionIndex()
    {
        $duplicate_serial = [];
        $dateToday = date('Ymd');
        $file = Yii::$app->basePath.'/web/csv/EuroTrk.csv';
        if(file_exists($file)) {
            $connection = Yii::$app->db;

            $row = 1;
            $serial_nos_csv = [];
            if (($handle = fopen($file, "r")) !== FALSE) {
               fgetcsv($handle);
               while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                   $serialNo = $data[0];
                   array_push($serial_nos_csv, $serialNo);
                   $record = Product::findOne($serialNo);
                   if(!empty($record)) {
                       array_push($duplicate_serial, $serialNo);
                   }
                }
                fclose($handle);
            }

            $model = new Product;
            $serial_nos = $model->find()
                       ->select('serial_no')
                       ->where([
                           'status'    => 'available',
                           'ignore_flag' => 0
                       ])
                       ->asArray()
                       ->all();
            $callBack = function($value) {
                return $value['serial_no'];
            };

            $arr = array_map($callBack, $serial_nos);
            $prod_to_delete = array_diff($arr, $serial_nos_csv);

            $sql = "LOAD DATA LOCAL INFILE '$file' IGNORE
                INTO TABLE product
                FIELDS TERMINATED BY ','
                ENCLOSED BY '\"' ESCAPED BY '\"'
                LINES TERMINATED BY '\\r\\n'
                IGNORE 1 LINES
                (serial_no, model, color, skirt, leds, full_foam, stereo, special, jets, frame, ext, @date, option_str, revision, order_sage_no)
                SET date_completed= (
                    CASE
                        WHEN @date REGEXP '[0-9]{2}/[0-9]{2}/[0-9]{4}' THEN STR_TO_DATE(@date,'%m/%d/%Y')
                    END
                )";

            if(!empty($file)) {
                $command=$connection->createCommand($sql);
                $command->execute();
            }

            $deleted = Product::deleteAll(['serial_no' => $prod_to_delete]);
        }
    }
}

?>
