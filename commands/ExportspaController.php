<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\BaseUrl;
use yii\helpers\Url;
use app\components\helpers\Data;
use yii\base\Exception;

use app\models\Product;

class ExportspaController extends Controller {

    public function actionIndex()
    {
        $dateToday = date('Ymd');
        $file = Yii::$app->basePath.'/web/csvexport/spa_csv'.$dateToday.'.csv';

        $spaModel = new Product;
        $items = $spaModel->getProducts();
		$output = fopen($file, 'w');
		$columnnames = array('SERIAL #','MODEL','COLOR','SKIRT','LED','FF','ST','Spec','SSJ',
                             'FRM','EXT','COMPLETED DATE', 'OPTIONSTRING', 'REVISION');

		fputcsv($output, $columnnames);
		foreach ($items as $fields) {
            $date = date_create($fields['date_completed']);
            $fields['date_completed'] = date_format($date, 'Y/m/d');
			fputcsv($output, $fields);
		}
		fclose($output);
    }
}


?>
