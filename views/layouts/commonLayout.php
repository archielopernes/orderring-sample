<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\BaseUrl;
use yii\helpers\Url;
use app\assets\CommonAsset;
use app\components\helpers\Users;

CommonAsset::register($this);
$identity = Users::initUser();

$items['admin'] = [
    [
        'label'    => 'Account Management',
        'url'      => [Yii::$app->request->baseUrl.'/account/manage']
    ],
    [
        'label'    => 'Spa Inventory',
        'url'      => [Yii::$app->request->baseUrl.'/spas/manage']
    ],
    [
        'label'    => 'Order Management',
        'url'      => [Yii::$app->request->baseUrl.'/order/manage']
    ]
];

$items['user'] = [
    [
        'label'    => Yii::t('app', 'Spas'),
        'url'      => [Yii::$app->request->baseUrl.'/spas/manage']
    ],
    [
        'label'    => Yii::t('app', 'My Orders'),
        'url'      => [Yii::$app->request->baseUrl.'/order/manage']
    ],
    [
        'label'    => Yii::t('app', 'Terms and Conditions'),
        'url'      => [Yii::$app->request->baseUrl.'/terms-conditions']
    ]
];

if($identity->user_type == 'user') {
    $accountItems = [
        [
            'label' => Yii::t('app', 'Profile'),
            'url'   => [Yii::$app->request->baseUrl.'/profile']
        ],
        [
            'label' => Yii::t('app', 'Change Password'),
            'url'   => [Yii::$app->request->baseUrl.'/changepassword']
        ],
        [
            'label' => Yii::t('app', 'Logout'),
            'url'   => [Yii::$app->request->baseUrl.'/logout']
        ],
    ];
} else {
    $accountItems = [
        [
            'label' => Yii::t('app', 'Change Password'),
            'url'   => [Yii::$app->request->baseUrl.'/changepassword']
        ],
        [
            'label' => Yii::t('app', 'Logout'),
            'url'   => [Yii::$app->request->baseUrl.'/logout']
        ],
    ];
}





?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="common-wrapper">
        <header>
            <nav class="navbar">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="/">
                    <img alt="MasterSpa" src='/resources/common/logo.png' class="img-responsive">
                  </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php if (!Yii::$app->user->isGuest) {
                        $name = $identity->username;
                        $nav = [
                            'options' => ['class' => 'nav navbar-nav navbar-right'],
                            'activateParents' => 'true',
                            'route' => empty(Yii::$app->controller->route_nav) ? Yii::$app->request->pathInfo : Yii::$app->controller->route_nav,
                            'items' => [
                                [
                                    'label'    => Yii::t('app', 'Dashboard'),
                                    'url'      => [Yii::$app->request->baseUrl.'/dashboard']
							    ],
                                //$items[$identity->user_type],
                                [
                                    'label'        => $name,
                                    'options'      => ['class'=>'dropdown'],
                                    'linkOptions'  => [
                                        'class'    => 'dropdown-toggle',
                                        'data-toggle' => 'dropdown'
                                    ],
                                    'items'		   => $accountItems
                                ]
                            ]
                        ];

                        $lastItem = array_pop($nav['items']);

                        foreach ($items[$identity->user_type] as $val) {
                            array_push($nav['items'], $val);
                        }

                        array_push($nav['items'], $lastItem);

                        echo Nav::widget($nav);
                     } else {
                        echo Nav::widget([
                            'options' => ['class' => 'nav navbar-nav navbar-right'],
                            'items' => [
                                [
                                    'label'    => Yii::t('app', 'Login'),
                                    'url'      => [Yii::$app->request->baseUrl.'/login'],
                                    'options'=> ['class'=>'active']
                                ]
                            ]
                        ]);
                    } ?>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
        </header>
        <div class="container-fluid common-container">
            <?= $content ?>
        </div>
    </div>

    <footer class="common-footer">
      <div id="footer" class="center-block text-center">
        Copyright &copy; <?= date('Y'); ?> APL. All rights reserved
      </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
