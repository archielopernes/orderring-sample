<?php
    use yii\helpers\Html;
    $this->title = 'Order Failed';
?>


<div style="padding-top: 2%; padding-bottom: 1%;">
    <img class="img-responsive center-block" src="/resources/common/logo.png" style="max-width: 300px;">
</div>

<div id="order-form">
    <div class="col-sm-6 col-sm-offset-3">
        <div id="order-header" class="text-center">
            <h3>Order Failed</h3>
        </div>
        <br>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Order Failed</h3>
            </div>
            <div class="panel-body">
                Sorry, The product you ordered was sold just now.  Sorry,  the product you ordered is no longer available.
            </div>
            <div class="panel-footer">
                <ul class="list-inline text-right">
                    <li>
                        <a href="/order/manage">
                            <button type="button" class="btn btn-info">Go to My Orders</button>
                        </a>
                    </li>
                    <li>
                        <a href="/spas/manage">
                            <button type="button" class="btn btn-warning">Buy Spas</button>
                        </a>
                    </li>
                </div>
            </div>
        </div>

    </div>
</div>
