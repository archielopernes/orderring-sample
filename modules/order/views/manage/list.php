<?php
    use app\components\helpers\Users;
    use app\assets\DatatablesAsset;
    use app\assets\InventoryAsset;
    use app\assets\MsgboxAsset;
    DatatablesAsset::register($this);
    InventoryAsset::register($this);
    MsgboxAsset::register($this);

    $this->title = 'Order List';
?>

<br>

<!-- Flash Messages -->
<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> <?= Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Error!</strong> <?= Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } ?>

<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered" id="order-list">
                <thead>
                    <tr>
                        <th class="text-center">ORDER ID</th>
                        <th class="text-center">SERIAL NO</th>
                        <th class="text-center">MODEL</th>
                        <th class="text-center">DATE ORDERED</th>
                        <th class="text-center">ORDER STATUS</th>
                        <th class="text-center">COVERS</th>
                        <th class="text-center">STEPS</th>
                        <th class="text-center">SAGE ID</th>
                        <?php if(Users::initUser()->user_type == 'admin') { ?>
                            <th class="text-center">CUSTOMER NAME</th>
                            <th></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($records as $value) : ?>
                    <tr>
                        <td class="text-center">E<?= $value['id'] ?></td>
                        <td class="text-center"><?= $value['serial_no'] ?></td>
                        <td class="text-center"><?= $value['model'] ?></td>
                        <td class="text-center"><?= $value['created_at'] ?></td>
                        <td class="text-center">
                            <?php if ($value['status'] != 'cancelled') {  ?>
                                <?php if($value['order_status'] == 'in_warehouse') {  ?>
                                    In Warehouse
                                <?php } else { ?>
                                    In Transit
                                <?php } ?>
                            <?php } else { ?>
                                Order Cancelled
                            <?php } ?>
                        </td>
                        <td class="text-center"><?= $value['cover'] ?></td>
                        <td class="text-center"><?= $value['step'] ?></td>
                        <td class="text-center"><?= $value['order_sage_no'] ?></td>
                        <?php if(Users::initUser()->user_type == 'admin') { ?>
                        <td class="text-center"><?= $value['full_name'] ?></td>
                        <td class="text-center">

                            <?php if($value['order_status'] == 'in_warehouse') { ?>
                                <button type="button" class="btn btn-warning warehouse-btn" id="<?= $value['id'] ?>">In Warehouse</button>
                            <?php } elseif($value['order_status'] == 'in_transit') { ?>
                                <button type="button" class="btn btn-success transit-btn" id="<?= $value['id'] ?>">In Transit</button>
                            <?php } ?>

                            <button class="btn btn-danger cancel-btn" id="<?= $value['id'] ?>"
                                    <?= ($value['status'] == 'cancelled') ? 'disabled' : ''; ?>>Cancel Order</button>

                            <?php if ($value['status'] == 'cancelled') { ?>
                                <button class="btn btn-danger delete-order" data-toggle="tooltip" data-placement="top" title="Delete Order" id="<?= $value['id'] ?>">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            <?php } ?>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php if(Users::initUser()->user_type == 'admin') { ?>
            <div class="panel-footer">
                <ul class="list-inline text-right">
                    <li>
                        <a href="/order/manage/orderexport" class="btn btn-warning">Export Order CSV</a>
                    </li>
                </ul>
            </div>
        <?php } ?>
    </div>
</div>
