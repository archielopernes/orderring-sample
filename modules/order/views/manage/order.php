<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    $this->title = 'Order';
?>

<?php $form = ActiveForm::begin([
    'id'      => 'order-form',
    'method'      => 'post',
]); ?>
<div style="padding-top: 2%; padding-bottom: 1%;">
    <img class="img-responsive center-block" src="/resources/common/logo.png" style="max-width: 300px;">
</div>

<div id="order-form">
    <div class="col-sm-6 col-sm-offset-3">
        <div id="order-header" class="text-center">
            <h3>Order Form</h3>
        </div>
        <br>
        <!-- Flash Messages -->
        <?php if (Yii::$app->session->hasFlash('error')) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> <?= Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php } ?>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Spa Information</h3>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>Serial Number</td>
                        <td><?= $record->serial_no ?></td>
                    </tr>
                    <tr>
                        <td>Model</td>
                        <td><?= $record->model ?></td>
                    </tr>
                    <tr>
                        <td>Color</td>
                        <td><?= $record->color ?></td>
                    </tr>
                    <tr>
                        <td>Skirt</td>
                        <td><?= $record->skirt ?></td>
                    </tr>
                    <tr>
                        <td>Full Foam</td>
                        <td><?= ($record->full_foam) ?  'Yes' : 'No' ?></td>
                    </tr>
                    <tr>
                        <td>Jets</td>
                        <td><?= $record->jets ?></td>
                    </tr>
                    <tr>
                        <td>LEDS</td>
                        <td><?= ($record->leds) ? 'Yes' : 'None' ?></td>
                    </tr>
                    <tr>
                        <td>Special</td>
                        <td><?= ($record->special) ? $record->special : 'None' ?></td>
                    </tr>
                    <tr>
                        <td>Stereo</td>
                        <td><?= ($record->stereo) ? $record->stereo : 'None' ?></td>
                    </tr>
                    <tr>
                        <td>Extended Warranty</td>
                        <td><?= ($record->ext) ? $record->ext : 'None' ?></td>
                    </tr>
                    <tr>
                        <td>Covers</td>
                        <td><?= $form->field($stepsCoversModel, 'covers')->dropDownList($listCoversData)->label(false) ?></td>
                    </tr>
                    <tr>
                        <td>Steps</td>
                        <td><?= $form->field($stepsCoversModel, 'steps')->dropDownList($listStepsData)->label(false) ?></td>
                    </tr>
                </table>
            </div>
            <div class="panel-footer">
                If you are sure you want to buy the spa listed above, click the "Buy Now" button at the bottom of the form. Your order will be emailed to our sales department and processed immediately. Thank you!
                <ul class="list-inline text-right">
                    <li>
                        <input type="hidden" name="serial_no" value="<?= $record->serial_no ?>">
                        <input type="hidden" name="order_sage_no" value="<?= $record->order_sage_no ?>">
                        <input type="submit" class="btn btn-info" value="Purchase Spa">
                    </li>
                    <li>
                        <a href="/spas/manage">
                            <button type="button" class="btn btn-default">Cancel</button>
                        </a>
                    </li>
                </div>
            </div>
        </div>

    </div>
</div>

<?php ActiveForm::end(); ?>
