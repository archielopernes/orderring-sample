<?php

namespace app\modules\order\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Order;
use app\models\Product;
use app\models\User;
use app\models\StepsCovers;
use app\components\helpers\Users;
use app\components\helpers\Data;

class ManageController extends \yii\web\Controller
{
    public $layout = '/emptyLayout';
    public $route_nav;
    public $viewPath = 'app/modules/order/views';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // Pages that are included in the rule set
                'only'  => ['index', 'confirm', 'list', 'cancel'],
                'rules' => [
                    [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['index', 'confirm', 'list', 'cancel'],
                        'roles'     => ['@'],
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    // Action when user is denied from accessing a page
                    if (Yii::$app->user->isGuest) {
                        $this->goHome();
                    } else {
                        $this->redirect(['/dashboard']);
                    }
                }
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init()
    {
        if(Yii::$app->user->isGuest) {
            $url = '/admin/login';
        } else {
            $url = '/admin/dashboard';
        }
    }

    public function actionIndex($serialNo)
    {
        $model = new Order;
        $record = Product::findOne($serialNo);
        $stepsCoversModel = new StepsCovers;

        if(empty($record)) {
            return $this->redirect('/spas/manage');
        } else if ($record->status != 'available') {
            return $this->redirect('/order/manage/failed');
        }

        if (substr($serialNo, 0, 1) === '1' || substr($serialNo, 0, 1) === 'R') {
            $qType = 'hot_tub';
        } else {
            $qType = 'swim_spa';
        }

        $steps = Data::findRecords($stepsCoversModel, ['id', 'name'], ['type' => 'steps', 'q_type' => $qType, 'status' => 'active'], 'all', 'true');
        $covers = Data::findRecords($stepsCoversModel, ['id', 'name'], ['type' => 'covers', 'q_type' => $qType, 'status' => 'active'], 'all', 'true');

        if(Yii::$app->request->isPost) {
            $order = Yii::$app->request->post('serial_no');
            $orderSageNo = Yii::$app->request->post('order_sage_no');
            $step = Yii::$app->request->post('StepsCovers')['steps'];
            $cover = Yii::$app->request->post('StepsCovers')['covers'];
            $identity = Users::initUser();

            $updatedRecord = Product::findOne($serialNo);

            if ($updatedRecord->status == 'available') {
                if($model->saveOrder($order, $step, $cover, $identity->id)) {
                    $mailBody = $this->generateMailBodyForOrder($orderSageNo);
                    if (Yii::$app->mailer->compose()
                        ->setCharset('UTF-8')
                        ->setFrom([Yii::$app->params['adminEmail'] => 'Master Spas'])
                        ->setTo($identity->email_address)
                        ->setSubject('Your Order Has Been Created')
                        ->setHtmlBody($mailBody)
                        ->send()) {
                        $mailBodyMaster = $this->generateMailBodyForOrderMaster($orderSageNo);
                        if (Yii::$app->mailer->compose()
                            ->setCharset('UTF-8')
                            ->setFrom([Yii::$app->params['adminEmail'] => 'Master Spas'])
                            // TODO ask for admin email address
                            ->setTo('fthompson@masterspas.com')
                            //->setTo('archielopernes@gmail.com')
                            ->setSubject('Your Order Has Been Created')
                            ->setHtmlBody($mailBodyMaster)
                            ->send()) {
                            return $this->redirect('/order/manage/confirm');
                        } else {
                            Yii::$app->session->setFlash('error', 'There was an error while processing your order. Please try again later.');
                        }
                    } else {
                        Yii::$app->session->setFlash('error', 'There was an error while processing your order. Please try again later.');
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'There was an error while processing your order. Please try again later.');
                }
            } else {
                return $this->redirect('/order/manage/failed');
            }
        }

       return $this->render('order', [
           'record' => $record,
           'stepsCoversModel' => $stepsCoversModel,
           'listStepsData' =>ArrayHelper::map($steps, 'id', 'name'),
           'listCoversData' => ArrayHelper::map($covers, 'id', 'name')
       ]);
    }

    public function actionConfirm()
    {
        return $this->render('order_successful');
    }

    public function actionFailed()
    {
        return $this->render('order_failed');
    }

    public function actionList()
    {
        $this->layout = '/commonLayout';
        $identity = Users::initUser();
        $model = new Order;

        $model->getOrder(false);

        if ($identity->user_type == 'admin') {
            $records = $model->getOrder(false);
        } else {
            $records = $model->getOrder(false, $identity->id);
        }

        return $this->render('list', [
            'records' => $records
        ]);
    }

    public function actionCancel($orderId)
    {
        $identity = Users::initUser();
        $model = new Order;
        $userModel = new User;
        $productModel = new Product;
        $orderInfo = Order::findOne($orderId);
        $productInfo = Product::findOne($orderInfo->serial_no);
        $userInfo = User::findOne($orderInfo->user_id);

        if($model->cancelOrder($orderId)) {
            $mailBody = $this->generateMailBodyForOrderCancel($orderInfo, $productInfo);
            if (Yii::$app->mailer->compose()
                    ->setCharset('UTF-8')
                    ->setFrom([Yii::$app->params['adminEmail'] => 'Master Spas'])
                    ->setTo($userInfo->email_address)
                    ->setSubject('Order Cancellation')
                    ->setHtmlBody($mailBody)
                    ->send()) {
                return '/order/manage';
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function actionChangetransit($orderId)
    {
        $model = Order::findOne($orderId);

        $model->order_status = 'in_transit';

        if($model->save()) {
            return '/order/manage';
        }

        return false;
    }

     public function actionChangewarehouse($orderId)
    {
        $model = Order::findOne($orderId);

        $model->order_status = 'in_warehouse';

        if($model->save()) {
            return '/order/manage';
        }

        return false;
    }

    public function actionOrderexport()
    {
		header('Content-Type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=order.csv');

        $orderModel = new Order;
        $items = $orderModel->getOrder(true);
        $flag = 0;
		$output = fopen('php://output', 'w');
		$columnnames = array('Serial Number','Customer Number','Cover / Steps', 'Order #', 'Order Status', 'Order Date Time', 'Option String', 'Revision', 'Order');

        $arr = ['model', 'cover', 'step'];
        $temp = [];
		fputcsv($output, $columnnames);
		foreach ($items as $fields) {
            $temp = [
                'model' => $fields['model'],
                'cover' => $fields['cover'],
                'step'  => $fields['step']
            ];
            if ($fields['status'] == 'cancelled') {
                $fields['order_status'] = 'Order Cancelled';
            } else {
                if ($fields['order_status'] == 'in_warehouse') {
                    $fields['order_status'] = 'In Warehouse';
                } else {
                    $fields['order_status'] = 'In Transit';
                }
            }

            unset($fields['step'], $fields['model']);
            foreach($arr as $val) {
                if($val != 'model') {
                    $fields['serial_no'] = null;
                    $fields['option_str'] = null;
                    $fields['revision'] = null;
                }

                $fields['cover'] = $temp[$val];
                unset($fields['user_id'], $fields['steps'], $fields['covers'], $fields['updated_at'], $fields['status'], $fields['full_name']);
                fputcsv($output, $fields);
            }
            $flag++;
		}
		fclose($output);

        $session = Yii::$app->session;
        $session->open();
        $session->set('flag_count_products', $flag);
        $session->close();
    }

    public function actionDelete($orderId)
    {
        $model = Order::findOne($orderId);
        if($model->delete()) {
            return '/order/manage';
        }

        return false;
    }

    private function generateMailBodyForOrderMaster($order)
    {
        $mailBody = '<html><head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
                    <title>Your MasterSPA Account Confirmation Link</title>
                    <style type="text/css">
                        div, p, a, li, td { -webkit-text-size-adjust:none; }
                        *{-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}
                        .ReadMsgBody {width: 100%; background-color: #ffffff;}
                        .ExternalClass {width: 100%; background-color: #ffffff;}
                        body {width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
                        html{width: 100%; background-color: #ffffff;}
                        p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important;}
                        .image180 img {width: 180px; height: auto;}
                        .image280 img {width: 280px; height: auto;}
                        .logo img {width: 180px; height: auto;}
                        .image215 img {width: 215px; height: auto;}
                        .image600 img {width: 600px; height: auto;}
                        .image400 img {width: 400px; height: auto;}
                        .image300 img {width: 300px; height: auto;}
                        a:link {text-decoration:none;}
                        a:hover {text-decoration:none;}
                        a:active {text-decoration:none;}
                        a:visited {text-decoration:none;}
                    </style>
                    <!-- @media only screen and (max-width: 640px) {*/-->
                    <style type="text/css">
                    @media only screen and (max-width: 640px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .fontSmall {font-size: 55px!important; line-height: 62px!important; letter-spacing: 8px!important;}
                        .h80 {height: 90px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 40px!important;}
                        table[class=scale25] {width: 23%!important;}
                        table[class=scale25Right] {width: 23%!important;}
                        table[class=scale33] {width: 30%!important;}
                        table[class=scale33Right] {width: 30%!important;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        table[class=w5] {width: 3%!important; }
                        table[class=w10] {width: 5%!important; }
                        .pad0 {padding: 0px!important;}
                        .image600 img {width: 100%!important;}
                        .image180 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                    <!--@media only screen and (max-width: 479px) {-->
                    <style type="text/css">
                    @media only screen and (max-width: 479px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .buttonScale {text-align: center!important; float: none!important;}
                        .fontSmall {font-size: 42px!important; line-height: 48px!important; letter-spacing: 5px!important;}
                        .fontSmall2 {font-size: 34px!important; line-height: 38px!important; letter-spacing: 2px!important;}
                        .h80 {height: 80px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 30px!important;}
                        table[class=scale25] {width: 100%!important; clear: both;}
                        table[class=scale25Right] {width: 100%!important; clear: both;}
                        table[class=scale33] {width: 100%!important; clear: both;}
                        table[class=scale33Right] {width: 100%!important; clear: both;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        .h40 {height: 40px!important;}
                        .pad0 {padding: 0px!important;}
                        .image180 img {width: 100%!important;}
                        .image600 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                </head>
                <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">
                <!-- Border -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: #0088cc;">
                    <tbody><tr>
                        <td width="100%" height="2" valign="top">
                        </td>
                    </tr>
                </tbody></table>
                <!--End Border -->

                <!-- Header  -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: rgb(35, 40, 43);">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #eeeeee;" class="headerBG">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                        <tbody><tr>
                            <td width="100%">
                                <!-- Start Nav -->
                                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                <tbody><tr>
                                    <td width="100%" height="25"></td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="middle" class="logo">

                <!-- Logo -->
                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center;" class="fullCenter">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #ffffff; border-width: 5px; border-color: #ffffff; border-radius:10px; padding:5px;" height="60" valign="middle" width="100%" class="fullCenter">
                        <img width="180" src="http://www.masterspas.com/img/ms-logo.png" alt="masterspa_logo" border="0">
                    </td>
                </tr>
                </tbody></table>

                                    </td>
                                </tr>
                                </tbody></table>
                                <!-- End Nav -->
                            </td>
                        </tr>
                        </tbody></table>

                        <!-- Space -->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                            <tbody><tr>
                                <td width="100%" height="15"></td>
                            </tr>
                        </tbody></table>
                        <!-- End Space -->

                    </td>
                </tr>
                </tbody></table>
                <!-- End Header -->

                <!-- Main Content -->
                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                <tbody>
                <tr>
                    <td valign="top" width="100%">
                        <table class="mobile" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tbody>
                        <tr>
                            <td>
                                <!-- Space -->
                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                <tbody>
                                <tr>
                                    <td height="30" width="100%"></td>
                                </tr>
                                </tbody>
                                </table>
                                <!-- End Space -->

                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                <tbody>
                                <tr>
                                    <td width="100%">
                                        <!-- Centered Title -->
                                        <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                        <tbody>
                                        <tr>
                    <td style="text-align: center; font-family: Helvetica,Arial,sans-serif; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold; font-size: 26px;" class="fullCenter" valign="middle" width="100%">
                    <!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><!--<![endif]-->An order has been successfully made !<!--[if !mso]><!--></span><!--<![endif]--></td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #000000; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
                        <p><!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;">Sage Order # : ' . $order . '</span><!--<![endif]--></p>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><br></span></div>
                        <div><br></div>
                        <div><br><br></div>
                    </td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100">
                        <tbody>
                        <tr>
                            <td style="border-bottom: 1px solid #e5e5e5;" align="center" height="1" width="100"></td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="15" width="100%"></td>
                                        </tr>
                                        </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
                </table>
                <!-- End Main Content -->

                </body></html>';

        return $mailBody;
    }


    private function generateMailBodyForOrder($order)
    {
        $mailBody = '<html><head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
                    <title>Your MasterSPAS Account Confirmation Link</title>
                    <style type="text/css">
                        div, p, a, li, td { -webkit-text-size-adjust:none; }
                        *{-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}
                        .ReadMsgBody {width: 100%; background-color: #ffffff;}
                        .ExternalClass {width: 100%; background-color: #ffffff;}
                        body {width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
                        html{width: 100%; background-color: #ffffff;}
                        p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important;}
                        .image180 img {width: 180px; height: auto;}
                        .image280 img {width: 280px; height: auto;}
                        .logo img {width: 180px; height: auto;}
                        .image215 img {width: 215px; height: auto;}
                        .image600 img {width: 600px; height: auto;}
                        .image400 img {width: 400px; height: auto;}
                        .image300 img {width: 300px; height: auto;}
                        a:link {text-decoration:none;}
                        a:hover {text-decoration:none;}
                        a:active {text-decoration:none;}
                        a:visited {text-decoration:none;}
                    </style>
                    <!-- @media only screen and (max-width: 640px) {*/-->
                    <style type="text/css">
                    @media only screen and (max-width: 640px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .fontSmall {font-size: 55px!important; line-height: 62px!important; letter-spacing: 8px!important;}
                        .h80 {height: 90px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 40px!important;}
                        table[class=scale25] {width: 23%!important;}
                        table[class=scale25Right] {width: 23%!important;}
                        table[class=scale33] {width: 30%!important;}
                        table[class=scale33Right] {width: 30%!important;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        table[class=w5] {width: 3%!important; }
                        table[class=w10] {width: 5%!important; }
                        .pad0 {padding: 0px!important;}
                        .image600 img {width: 100%!important;}
                        .image180 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                    <!--@media only screen and (max-width: 479px) {-->
                    <style type="text/css">
                    @media only screen and (max-width: 479px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .buttonScale {text-align: center!important; float: none!important;}
                        .fontSmall {font-size: 42px!important; line-height: 48px!important; letter-spacing: 5px!important;}
                        .fontSmall2 {font-size: 34px!important; line-height: 38px!important; letter-spacing: 2px!important;}
                        .h80 {height: 80px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 30px!important;}
                        table[class=scale25] {width: 100%!important; clear: both;}
                        table[class=scale25Right] {width: 100%!important; clear: both;}
                        table[class=scale33] {width: 100%!important; clear: both;}
                        table[class=scale33Right] {width: 100%!important; clear: both;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        .h40 {height: 40px!important;}
                        .pad0 {padding: 0px!important;}
                        .image180 img {width: 100%!important;}
                        .image600 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                </head>
                <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">
                <!-- Border -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: #0088cc;">
                    <tbody><tr>
                        <td width="100%" height="2" valign="top">
                        </td>
                    </tr>
                </tbody></table>
                <!--End Border -->

                <!-- Header  -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: rgb(35, 40, 43);">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #eeeeee;" class="headerBG">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                        <tbody><tr>
                            <td width="100%">
                                <!-- Start Nav -->
                                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                <tbody><tr>
                                    <td width="100%" height="25"></td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="middle" class="logo">

                <!-- Logo -->
                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center;" class="fullCenter">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #ffffff; border-width: 5px; border-color: #ffffff; border-radius:10px; padding:5px;" height="60" valign="middle" width="100%" class="fullCenter">
                        <img width="180" src="http://www.masterspas.com/img/ms-logo.png" alt="masterspa_logo" border="0">
                    </td>
                </tr>
                </tbody></table>

                                    </td>
                                </tr>
                                </tbody></table>
                                <!-- End Nav -->
                            </td>
                        </tr>
                        </tbody></table>

                        <!-- Space -->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                            <tbody><tr>
                                <td width="100%" height="15"></td>
                            </tr>
                        </tbody></table>
                        <!-- End Space -->

                    </td>
                </tr>
                </tbody></table>
                <!-- End Header -->

                <!-- Main Content -->
                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                <tbody>
                <tr>
                    <td valign="top" width="100%">
                        <table class="mobile" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tbody>
                        <tr>
                            <td>
                                <!-- Space -->
                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                <tbody>
                                <tr>
                                    <td height="30" width="100%"></td>
                                </tr>
                                </tbody>
                                </table>
                                <!-- End Space -->

                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                <tbody>
                                <tr>
                                    <td width="100%">
                                        <!-- Centered Title -->
                                        <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                        <tbody>
                                        <tr>
                    <td style="text-align: center; font-family: Helvetica,Arial,sans-serif; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold; font-size: 26px;" class="fullCenter" valign="middle" width="100%">
                    <!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><!--<![endif]-->Congratulations ! Your Order Has Been Received !<!--[if !mso]><!--></span><!--<![endif]--></td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #000000; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
                        <p><!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;">Sage Order # : ' . $order . '</span><!--<![endif]--></p>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><br></span></div>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;">Thank you for ordering at masterspas.</span></div>
                        <div><br></div>
                        <div><br></div>
                        <div><br><br></div>
                    </td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100">
                        <tbody>
                        <tr>
                            <td style="border-bottom: 1px solid #e5e5e5;" align="center" height="1" width="100"></td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="15" width="100%"></td>
                                        </tr>
                                        </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
                </table>
                <!-- End Main Content -->

                </body></html>';

        return $mailBody;
    }

    private function generateMailBodyForOrderCancel($order, $productModel)
    {
        $mailBody = '<html><head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
                    <title>Your MasterSPA Account Confirmation Link</title>
                    <style type="text/css">
                        div, p, a, li, td { -webkit-text-size-adjust:none; }
                        *{-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}
                        .ReadMsgBody {width: 100%; background-color: #ffffff;}
                        .ExternalClass {width: 100%; background-color: #ffffff;}
                        body {width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
                        html{width: 100%; background-color: #ffffff;}
                        p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important;}
                        .image180 img {width: 180px; height: auto;}
                        .image280 img {width: 280px; height: auto;}
                        .logo img {width: 180px; height: auto;}
                        .image215 img {width: 215px; height: auto;}
                        .image600 img {width: 600px; height: auto;}
                        .image400 img {width: 400px; height: auto;}
                        .image300 img {width: 300px; height: auto;}
                        a:link {text-decoration:none;}
                        a:hover {text-decoration:none;}
                        a:active {text-decoration:none;}
                        a:visited {text-decoration:none;}
                    </style>
                    <!-- @media only screen and (max-width: 640px) {*/-->
                    <style type="text/css">
                    @media only screen and (max-width: 640px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .fontSmall {font-size: 55px!important; line-height: 62px!important; letter-spacing: 8px!important;}
                        .h80 {height: 90px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 40px!important;}
                        table[class=scale25] {width: 23%!important;}
                        table[class=scale25Right] {width: 23%!important;}
                        table[class=scale33] {width: 30%!important;}
                        table[class=scale33Right] {width: 30%!important;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        table[class=w5] {width: 3%!important; }
                        table[class=w10] {width: 5%!important; }
                        .pad0 {padding: 0px!important;}
                        .image600 img {width: 100%!important;}
                        .image180 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                    <!--@media only screen and (max-width: 479px) {-->
                    <style type="text/css">
                    @media only screen and (max-width: 479px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .buttonScale {text-align: center!important; float: none!important;}
                        .fontSmall {font-size: 42px!important; line-height: 48px!important; letter-spacing: 5px!important;}
                        .fontSmall2 {font-size: 34px!important; line-height: 38px!important; letter-spacing: 2px!important;}
                        .h80 {height: 80px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 30px!important;}
                        table[class=scale25] {width: 100%!important; clear: both;}
                        table[class=scale25Right] {width: 100%!important; clear: both;}
                        table[class=scale33] {width: 100%!important; clear: both;}
                        table[class=scale33Right] {width: 100%!important; clear: both;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        .h40 {height: 40px!important;}
                        .pad0 {padding: 0px!important;}
                        .image180 img {width: 100%!important;}
                        .image600 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                </head>
                <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">
                <!-- Border -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: #0088cc;">
                    <tbody><tr>
                        <td width="100%" height="2" valign="top">
                        </td>
                    </tr>
                </tbody></table>
                <!--End Border -->

                <!-- Header  -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: rgb(35, 40, 43);">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #eeeeee;" class="headerBG">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                        <tbody><tr>
                            <td width="100%">
                                <!-- Start Nav -->
                                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                <tbody><tr>
                                    <td width="100%" height="25"></td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="middle" class="logo">

                <!-- Logo -->
                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center;" class="fullCenter">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #ffffff; border-width: 5px; border-color: #ffffff; border-radius:10px; padding:5px;" height="60" valign="middle" width="100%" class="fullCenter">
                        <img width="180" src="http://www.masterspas.com/img/ms-logo.png" alt="masterspa_logo" border="0">
                    </td>
                </tr>
                </tbody></table>

                                    </td>
                                </tr>
                                </tbody></table>
                                <!-- End Nav -->
                            </td>
                        </tr>
                        </tbody></table>

                        <!-- Space -->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                            <tbody><tr>
                                <td width="100%" height="15"></td>
                            </tr>
                        </tbody></table>
                        <!-- End Space -->

                    </td>
                </tr>
                </tbody></table>
                <!-- End Header -->

                <!-- Main Content -->
                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                <tbody>
                <tr>
                    <td valign="top" width="100%">
                        <table class="mobile" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tbody>
                        <tr>
                            <td>
                                <!-- Space -->
                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                <tbody>
                                <tr>
                                    <td height="30" width="100%"></td>
                                </tr>
                                </tbody>
                                </table>
                                <!-- End Space -->

                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                <tbody>
                                <tr>
                                    <td width="100%">
                                        <!-- Centered Title -->
                                        <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                        <tbody>
                                        <tr>
                    <td style="text-align: center; font-family: Helvetica,Arial,sans-serif; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold; font-size: 26px;" class="fullCenter" valign="middle" width="100%">
                    <!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><!--<![endif]-->Your order has been cancelled !<!--[if !mso]><!--></span><!--<![endif]--></td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #000000; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
                        <span style="font-family: "Open Sans", Helvetica; font-weight: normal;">Sage Order # : ' . $productModel->order_sage_no . '</span><br><br>
                        <span style="font-family: "Open Sans", Helvetica; font-weight: normal;">Serial No : ' . $order->serial_no . '</span><br><br>
                        <span style="font-family: "Open Sans", Helvetica; font-weight: normal;">Model : ' . $productModel->model . '</span>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><br></span></div>
                        <div><br></div>
                        <div><br><br></div>
                    </td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100">
                        <tbody>
                        <tr>
                            <td style="border-bottom: 1px solid #e5e5e5;" align="center" height="1" width="100"></td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="15" width="100%"></td>
                                        </tr>
                                        </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
                </table>
                <!-- End Main Content -->

                </body></html>';

        return $mailBody;
    }
}

?>
