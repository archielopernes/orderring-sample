<?php
    use app\components\helpers\Users;
    use app\assets\DatatablesAsset;
    use app\assets\MsgboxAsset;
    DatatablesAsset::register($this);
    MsgboxAsset::register($this);

    $this->title = 'Account List';
?>

<br>

<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered" id="account-list">
                <thead>
                    <tr>
                        <th class="text-center">CUSTOMER NO</th>
                        <th class="text-center">CUSTOMER NAME</th>
                        <th class="text-center">USERNAME</th>
                        <th class="text-center">EMAIL ADDRESS</th>
                        <th class="text-center">REGISTRATION DATE</th>
                        <th class="text-center">STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($records as $value) : ?>
                    <tr>
                        <td class="text-center"><?= $value['customer_no'] ?></td>
                        <td class="text-center"><?= $value['full_name'] ?></td>
                        <td class="text-center"><?= $value['username'] ?></td>
                        <td class="text-center"><?= $value['email_address'] ?></td>
                        <td class="text-center"><?= $value['ins_time'] ?></td>
                        <td class="text-center"><?= ucfirst($value['status']) ?></td>
                        <td class="text-center">
                            <ul class="list-inline">
                                <?php if ($value['status'] === 'unconfirmed') { ?>
                                <li>
                                    <button type="button" class="btn btn-success activate-account-btn" id="<?= $value['id'] ?>"
                                            data-toggle="tooltip" data-placement="top" title="Confirm Account">
                                        <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
                                    </button>
                                </li>
                                <?php } ?>
                                <li>
                                    <a  type="button" class="btn btn-warning" href="/account/manage/cpass/<?= $value['id'] ?>"
                                       data-toggle="tooltip" data-placement="top" title="Change Password">
                                        <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                                    </a>
                                </li>
                                <li>
                                    <a type="button" class="btn btn-primary" href="/account/manage/edit/<?= $value['id'] ?>"
                                       data-toggle="tooltip" data-placement="top" title="Edit Account">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    </a>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-danger delete-account-btn" id="<?= $value['id'] ?>"
                                            data-toggle="tooltip" data-placement="top" title="Delete Account">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="panel-footer text-right">
            <a href="/account/manage/register">
                <button type="button" class="btn btn-info">Add Account</button>
            </a>
        </div>
    </div>
</div>
