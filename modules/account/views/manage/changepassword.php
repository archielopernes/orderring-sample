<?php
    use yii\bootstrap\ActiveForm;
    use app\assets\MsgboxAsset;
    MsgboxAsset::register($this);

    $this->title = 'Change Password';
?>

<br>
<!-- Flash Messages -->
<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> <?= Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Error!</strong> <?= Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } ?>

<?php $form = ActiveForm::begin([
        'id'        => 'changepassword-form',
        'method'    => 'post',
        'layout'    => 'horizontal'
    ]);
?>
<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">
            <?php if(!isset($admin)) { ?>
                <?= $form->field($model, 'oldPassword')->passwordInput() ?>
            <?php } ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'confirmPassword')->passwordInput() ?>
        </div>
        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-info">Change Password</button>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

