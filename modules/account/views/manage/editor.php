<?php
    use yii\bootstrap\ActiveForm;
    use app\assets\MsgboxAsset;
    MsgboxAsset::register($this);

    $this->title = 'Add Account';
?>

<br>
<?php $form = ActiveForm::begin([
        'id'        => 'register-form',
        'method'    => 'post',
        'layout'    => 'horizontal'
    ]);
?>
<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">

            <?= $form->field($model2, 'first_name')->textInput(array('placeholder'=>'First Name')) ?>
            <?= $form->field($model2, 'last_name')->textInput(array('placeholder'=>'Last Name')) ?>
            <?= $form->field($model, 'customer_no')->textInput(array('placeholder'=>'Customer No.')) ?>
            <?= $form->field($model, 'dealer')->textInput(array('placeholder'=>'Dealer')) ?>
            <?= $form->field($model, 'email_address')->textInput(array('placeholder'=>'Email Address')) ?>
            <?= $form->field($model, 'username')->textInput(array('placeholder'=>'Username')) ?>
            <?php if (!isset($id)) { ?>
                <?= $form->field($model, 'password')->passwordInput(array('placeholder'=>'Password', 'value' => '')) ?>
                <?= $form->field($model, 'confirmPassword')->passwordInput(array('placeholder'=>'Confirm Password')) ?>
            <?php } ?>
        </div>
        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-info">Save Account</button>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

