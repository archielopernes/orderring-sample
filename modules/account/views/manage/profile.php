<?php
    use app\components\helpers\Users;

    $this->title = 'Profile';
?>

<br>

<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">
            <div class="col-md-6 col-md-offset-3">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td width="25%"><strong>Name</strong></td>
                            <td><?= ucfirst($model2->first_name).' '.ucfirst($model2->last_name); ?></td>
                        </tr>
                         <tr>
                            <td width="25%"><strong>Username</strong></td>
                            <td><?= ucfirst($model->username) ?></td>
                        </tr>
                        <tr>
                            <td width="25%"><strong>Email</strong></td>
                            <td><?= ucfirst($model->email_address) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer text-right">
            <a href="/account/manage/edit/<?= $model->id ?>">
                <button type="button" class="btn btn-info">Edit Profile</button>
            </a>
        </div>
    </div>
</div>
