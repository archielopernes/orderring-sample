<?php
    use app\components\helpers\Users;
    $identity = Users::initUser();

    $this->title = 'Dashboard';
?>

<br>

<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">
            <?php if ($identity->user_type == 'admin') { ?>
                <div class="col-sm-2 text-center">
                    <a href="/account/manage">
                        <img class="img-responsive center-block" src="/resources/common/account.png">
                    </a> <br>
                    <a href="/account/manage">Account Management</a>
                </div>
            <?php } ?>

            <?php if ($identity->user_type == 'admin') { ?>
                <div class="col-sm-2 text-center">
                    <a href="/stepscovers/manage">
                        <img class="img-responsive center-block" src="/resources/common/tools.png">
                    </a> <br>
                    <a href="/stepscovers/manage">Steps / Covers Management</a>
                </div>
            <?php } ?>

            <div class="col-sm-2 text-center">
                <a href="/spas/manage">
                    <img class="img-responsive center-block" src="/resources/common/product.png">
                </a> <br>
                <a href="/spas/manage"><?= ($identity->user_type == 'admin') ? 'Spa Inventory' : 'Spas'; ?></a>
            </div>

            <div class="col-sm-2 text-center">
                <a href="/order/manage">
                    <img class="img-responsive center-block" src="/resources/common/order.png">
                </a> <br>
                <a href="/order/manage"><?= ($identity->user_type == 'admin') ? 'Order Management' : 'My Orders'; ?></a>
            </div>

            <?php if ($identity->user_type == 'user') { ?>
                <div class="col-sm-2 text-center">
                    <a href="/profile">
                        <img class="img-responsive center-block" src="/resources/common/account.png">
                    </a> <br>
                    <a href="/profile">Profile</a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
