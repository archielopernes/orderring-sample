<?php
/**
*   @created  	2014-12-05
*/

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if($resetIndex === 1) {
	$this->title = Yii::t('app', 'Reset Password');
} elseif($resetIndex === 2) {
	$this->title = Yii::t('app', 'Forgotten Password');
} elseif($resetIndex === 3) {
	$this->title = Yii::t('app', 'Change Password');
} else {
	$this->title = '';
}

?>
<!-- Main Content -->
<div id="login-wrapper">
    <div id="logo">
        <img src='/resources/common/logo.png' class="img-responsive">
    </div>
    <div id="content">

<?php
    if ($resetMessage === 1) {
        echo '<div class="alert alert-success">';
        echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
        echo '<strong>' . Yii::t('app', 'Success!') . ' </strong>';
        echo Yii::t('app', 'Password reset link has been sent to your email address.');
        echo '</div>';
    } elseif ($resetMessage === 2) {
        echo '<div class="alert alert-danger">';
        echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
        echo '<strong>' . Yii::t('app', 'Error!') . ' </strong>';
        echo Yii::t('app', 'There was an error in sending the mail.');
        echo '</div>';
    } elseif ($resetMessage === 3) {
        echo '<div class="alert alert-warning">';
        echo '<a href="/account/settings/resetpassword" class="close">' . Yii::t('app', 'OK') . '</a>';
        echo '<strong>' . Yii::t('app', 'Warning!') . ' </strong>';
        echo Yii::t('app', 'Reset link is corrupted.');
        echo '</div>';
    } elseif ($resetMessage === 4) {
        echo '<div class="alert alert-danger">';
        echo '<a href="/account/settings/resetpassword" class="close">' . Yii::t('app', 'OK') . '</a>';
        echo '<strong>' . Yii::t('app', 'Error!') . ' </strong>';
        echo Yii::t('app', 'Link has expired or is invalid.');
        echo '</div>';
    } elseif ($resetMessage === 5) {
        echo '<div class="alert alert-success">';
        echo '<a href="/login" class="close">' . Yii::t('app', 'Login') . '</a>';
        echo '<strong>' . Yii::t('app', 'Success!') . ' </strong>';
        echo Yii::t('app', 'Password has been reset.');
        echo '</div>';
    } elseif ($resetMessage === 6) {
        echo '<div class="alert alert-success">';
        echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
        echo '<strong>' . Yii::t('app', 'Success!') . ' </strong>';
        echo Yii::t('app', 'Password has been changed.');
        echo '</div>';
    }
?>
    <?php if ($resetIndex > 0) { ?>
    <h2 class="title"><?= Html::encode($this->title) ?></h2><div class="clearfix"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="span12 item-block">
                <?php if($resetIndex === 1) { ?>
                    <div class="container-fluid" >
                            <?php $form = ActiveForm::begin([
                                    'id' 			=> 'resetPassword-form',
                                    'method'    	=> 'post',
                                    'options' 		=> ['class' => 'form-horizontal'],
                                    'fieldConfig' => [
                                            'template' => "<div class=\"control-group\">{input}</div>\n<div>{error}</div>"
                                    ]
                                  ]); ?>
                                        <?= $form->field($userModel, 'password')->passwordInput(array('placeholder'=>'Password')) ?>
                                        <?= $form->field($userModel, 'confirmPassword')->passwordInput(array('placeholder'=>'Confirm Password')) ?>
                                        <div class="form-group container-fluid">
                                            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'resetPassword-button']) ?>
                                        </div>
                            <?php ActiveForm::end(); ?>
                    </div>
                <?php } elseif($resetIndex === 2) { ?>
                    <div class="container-fluid" >
                            <?php $form = ActiveForm::begin([
                                    'id'			=> 'forgottenPassword-form',
                                    'method'    	=> 'post',
                                    'options' 		=> ['class' => 'form-horizontal'],
                                    'fieldConfig' => [
                                            'template' => "<div class=\"control-group\">{input}</div>\n<div>{error}</div>"
                                    ]
                                  ]); ?>
                                        <?= $form->field($userModel, 'email_address')->textInput(array('placeholder'=>'Email Address')) ?>
                                        <div class="form-group container-fluid">
                                            <a href="/" class="btn btn-danger pull-left">Cancel</a>
                                            <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary pull-right', 'name' => 'forgottenPassword-button']) ?>
                                        </div>
                            <?php ActiveForm::end(); ?>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
    <?php } ?>
    </div>
</div>
<!-- Main Content End -->
