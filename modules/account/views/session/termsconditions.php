<?php
    use app\components\helpers\Users;
    use app\assets\DatatablesAsset;
    use app\assets\InventoryAsset;
    use app\assets\MsgboxAsset;
    DatatablesAsset::register($this);
    InventoryAsset::register($this);
    MsgboxAsset::register($this);

    $this->title = 'Terms and Conditions';
?>

<br>

<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">
            <ul class="text-left">
                <li>Product ordered using the Online European Warehouse Ordering System must be paid for within 4 days of selection. </li>
                <li>Confirmation of a wire transfer must be received by Master Spas by the end of the 4th business day.</li>
                <li>Failure to send confirmation within this time will result in that product being automatically returned to the stock list.</li>
                <li>Product must be taken within 10 business days of initial selection or a $20 per day warehouse fee will accrue.</li>
                <li>Cancellation of any order after payment has been made will result in a credit equal to the payment made being applied to your account less a $50 administrative fee.</li>
                <li>If Cancellation is made after 10 days, a 10% restock fee will also be deducted from the credit.</li>
            </ul>
        </div>
        <div class="panel-footer">
            <ul class="list-inline text-right">
                <li>
                    <a href="/" class="btn btn-danger">Back</a>
                </li>
            </ul>
        </div>
    </div>
</div>
