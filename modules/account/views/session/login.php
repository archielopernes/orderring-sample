<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    $this->title = 'Login';
?>

<div id="login-wrapper">
    <div id="logo">
        <img src='/resources/common/logo.png' class="img-responsive">
    </div>
    <div id="content">
        <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'method'    => 'post',
                    'fieldConfig' => [
                            'template' => "<div class=\"control-group\">{input}</div>\n<div>{error}</div>"
                    ]]);
        ?>

        <?= $form->field($model, 'username')->textInput(array('placeholder'=>'Username')) ?>

        <?= $form->field($model, 'password')->passwordInput(array('placeholder'=>'Password')) ?>

        <input type="submit" class="btn btn-primary btn-full" name="login-button" value="Login" style="width: 100%">
        <br><br>
        <div class="row">
<!--
            <div class="col-xs-12 col-sm-12 col-md-6">
                <a href="/register">Register</a>
            </div>
-->
            <div class="col-xs-12 col-sm-12 col-md-12    text-right">
                <a href="/forgotpassword">Forgot Password?</a>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>


