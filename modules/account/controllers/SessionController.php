<?php

namespace app\modules\account\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\User;
use app\models\UserInfo;
use app\models\LoginForm;
use yii\helpers\Url;
use app\components\helpers\Data;

class SessionController extends \yii\web\Controller
{
    public $layout = '/loginLayout';
    public $route_nav;
    public $viewPath = 'app/modules/account/views';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // Pages that are included in the rule set
                'only'  => ['dashboard', 'login', 'logout'],
                'rules' => [
                    [ // Pages that can be accessed without logging in
                        'allow'     => true,
                        'actions'   => ['login'],
                        'roles'     => ['?']
                    ],
                    [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['logout', 'dashboard'],
                        'roles'     => ['@']
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    // Action when user is denied from accessing a page
                    if (Yii::$app->user->isGuest) {
                        $this->goHome();
                    } else {
                        $this->redirect(['/dashboard']);
                    }
                }
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init()
    {
        if(Yii::$app->user->isGuest) {
            $url = '/login';
        } else {
            $url = '/dashboard';
        }
    }

    public function actionLogin()
    {
        $model = new LoginForm;

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = Yii::$app->user->getIdentity()->attributes;
            $session = Yii::$app->session;
            $session->open();
            foreach ($user as $userKey => $userValue) {
                $session[$userKey] = $userValue;
            }
            $session->close();

            $this->redirect(['/dashboard']);
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->removeAll();          // Removes all the session variables
        $session->destroy();            // Destroy session
        Yii::$app->response->clear();   // Clears the headers, cookies, content, status code of the response.
        Yii::$app->user->logout();
        $this->goHome();
    }

    public function actionDashboard()
    {
        $this->layout = '/commonLayout';
        return $this->render('dashboard');
    }

    public function actionForgotpassword()
    {
        // Initialize Model
        $userModel = new User;

        // Initialize Variables
        /*
            $resetIndex
            Where:
                0 = No display
                1 = Reset password form
                2 = Send reset link in email form (default)
        */
        $resetIndex   = 2;
        /*
            $resetMessage
            Where:
                0 = No message (default)
                1 = Reset link successfully sent to mail
                2 = Error in sending mail
                3 = Reset link is corrupt
                4 = Reset link has expired or is invalid
                5 = Password has been reset
                6 = Password has been changed
        */
        $resetMessage = 0;

        // Reset link should provide these values
        if (isset($_GET['u'], $_GET['t'])) {
            $resetIndex  = 0;
            // Decrypt hashed data
            $userId         = Yii::$app->getSecurity()->validateData($_GET['u'], 'userId');
            $expirationTime = Yii::$app->getSecurity()->validateData($_GET['t'], 'expirationTime');
            // Decryption will return an empty value if hash is tampered with
            if (empty($userId) || empty($expirationTime)) {
                $resetMessage = 3;
            } else {
                $expirationTimeDiff = Yii::$app->formatter->asDatetime($expirationTime);
                $currentTimeDiff    = Yii::$app->formatter->asDatetime('now');
                // Check if user and credential are valid or if expiration time has passed
                $params = ['id' => $userId];
                if ((count(Data::findRecords($userModel, null, $params)) <= 0)
                    || ($expirationTimeDiff <=  $currentTimeDiff)) {
                    $resetMessage = 4;
                } else {
                    // If there are no errors with the reset link, change password form will be displayed.
                    $resetIndex = 1;
                }
            }
        }

        // When Send/Save button is clicked and there are no validation errors, data will be posted.
        if ($userModel->load(Yii::$app->request->post())) {
            $emailAddress   = $userModel->email_address;
            $password       = $userModel->password;
            $params         = ['email_address' => $emailAddress, 'status' => 'active'];
            $doesEmailExist = Data::findRecords($userModel, null, $params);
            // If email address is valid and exists in database, reset email will be generated and sent.
            if (!empty($emailAddress) && count($doesEmailExist) > 0) {
                $mailBody = $this->generateResetPasswordMail($doesEmailExist);
                if (Yii::$app->mailer->compose()
                    ->setCharset('UTF-8')
                    ->setFrom([Yii::$app->params['adminEmail'] => 'Master Spas'])
                    ->setTo($userModel->email_address)
                    ->setSubject('Forgot Password')
                    ->setHtmlBody($mailBody)
                    ->send()) {
                    $userModel = new User;
                    $resetMessage = 1;
                } else {
                    $resetMessage = 2;
                }
            } else {
                $userModel->addError(
                    'email_address',
                    Yii::t('app', '{attribute} does not exist.', ['attribute' => Yii::t('app', 'Mail Address')])
                );
            }
            // If password is posted without validation errors, it is updated.
            if (!empty($password)) {
                $passwordHash = Yii::$app->getSecurity()->generatePasswordHash($password);
                $params = ['password' => $passwordHash, 'up_time' => Yii::$app->formatter->asDatetime('now')];
                $resetPassword = $userModel->updateRecord($userId, $params);
                if ($resetPassword) {
                    $userModel = new User;
                    $resetIndex   = 0;
                    $resetMessage = 5;
                }
            } else {
                $userModel->addError(
                    'password',
                    Yii::t('app', '{attribute} cannot be blank.', ['attribute' => Yii::t('app', 'Password')])
                );
            }
        }

        return $this->render(
            'forgotpassword',
            ['resetIndex'   => intval($resetIndex),
             'resetMessage' => $resetMessage,
             'userModel' => $userModel]
        );
    }

    public function actionRegister()
    {
        $model = new User;
        $model2 = new UserInfo;
        $model->scenario = 'registerAccount';
        $message = 0;

        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                if ($model->saveRecord(Yii::$app->request->post('User'), Yii::$app->request->post('UserInfo'))) {
                    $mailBody = $this->generateMailBody($model);
                    if (Yii::$app->mailer->compose()
                                            ->setCharset('UTF-8')
                                            ->setFrom([Yii::$app->params['adminEmail'] => 'Master Spas'])
                                            ->setTo($model->email_address)
                                            ->setSubject('Registration')
                                            ->setHtmlBody($mailBody)
                                            ->send()) {
                        Yii::$app->session->setFlash('success', 'You have successfully registered. Please wait for admin to confirm your account to login</a>.');
                    } else {
                        Yii::$app->session->setFlash('error', 'There was an error while registering your account. Please try again later.');
                    }

                } else {
                    Yii::$app->session->setFlash('error', 'There was an error while registering your account. Please try again later.');
                }

                return $this->refresh();
            } else {
                $errors = $model->getErrors();
                foreach ($errors as $key => $error) {
                    $save = false;
                    $model->addError($key, $error[0]);
                }
            }
        }

        return $this->render('register', [
            'model'     => $model,
            'model2'    => $model2,
            'message'   => $message
        ]);
    }

    public function actionTermsconditions()
    {
        $this->layout = '/commonLayout';
        return $this->render('termsconditions');
    }

    private function generateMailBody($account)
    {
        $mailBody = '';
        $userId = Yii::$app->getSecurity()->hashData($account->username, 'account');
        $confirmationLink  = Url::canonical() . '?u=' . $userId;
        $mailBody .= '<html><head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
                    <title>Your MasterSPA Account Confirmation Link</title>
                    <style type="text/css">
                        div, p, a, li, td { -webkit-text-size-adjust:none; }
                        *{-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}
                        .ReadMsgBody {width: 100%; background-color: #ffffff;}
                        .ExternalClass {width: 100%; background-color: #ffffff;}
                        body {width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
                        html{width: 100%; background-color: #ffffff;}
                        p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important;}
                        .image180 img {width: 180px; height: auto;}
                        .image280 img {width: 280px; height: auto;}
                        .logo img {width: 180px; height: auto;}
                        .image215 img {width: 215px; height: auto;}
                        .image600 img {width: 600px; height: auto;}
                        .image400 img {width: 400px; height: auto;}
                        .image300 img {width: 300px; height: auto;}
                        a:link {text-decoration:none;}
                        a:hover {text-decoration:none;}
                        a:active {text-decoration:none;}
                        a:visited {text-decoration:none;}
                    </style>
                    <!-- @media only screen and (max-width: 640px) {*/-->
                    <style type="text/css">
                    @media only screen and (max-width: 640px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .fontSmall {font-size: 55px!important; line-height: 62px!important; letter-spacing: 8px!important;}
                        .h80 {height: 90px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 40px!important;}
                        table[class=scale25] {width: 23%!important;}
                        table[class=scale25Right] {width: 23%!important;}
                        table[class=scale33] {width: 30%!important;}
                        table[class=scale33Right] {width: 30%!important;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        table[class=w5] {width: 3%!important; }
                        table[class=w10] {width: 5%!important; }
                        .pad0 {padding: 0px!important;}
                        .image600 img {width: 100%!important;}
                        .image180 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                    <!--@media only screen and (max-width: 479px) {-->
                    <style type="text/css">
                    @media only screen and (max-width: 479px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .buttonScale {text-align: center!important; float: none!important;}
                        .fontSmall {font-size: 42px!important; line-height: 48px!important; letter-spacing: 5px!important;}
                        .fontSmall2 {font-size: 34px!important; line-height: 38px!important; letter-spacing: 2px!important;}
                        .h80 {height: 80px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 30px!important;}
                        table[class=scale25] {width: 100%!important; clear: both;}
                        table[class=scale25Right] {width: 100%!important; clear: both;}
                        table[class=scale33] {width: 100%!important; clear: both;}
                        table[class=scale33Right] {width: 100%!important; clear: both;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        .h40 {height: 40px!important;}
                        .pad0 {padding: 0px!important;}
                        .image180 img {width: 100%!important;}
                        .image600 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                </head>
                <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">
                <!-- Border -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: #0088cc;">
                    <tbody><tr>
                        <td width="100%" height="2" valign="top">
                        </td>
                    </tr>
                </tbody></table>
                <!--End Border -->

                <!-- Header  -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: rgb(35, 40, 43);">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #eeeeee;" class="headerBG">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                        <tbody><tr>
                            <td width="100%">
                                <!-- Start Nav -->
                                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                <tbody><tr>
                                    <td width="100%" height="25"></td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="middle" class="logo">

                <!-- Logo -->
                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center;" class="fullCenter">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #ffffff; border-width: 5px; border-color: #ffffff; border-radius:10px; padding:5px;" height="60" valign="middle" width="100%" class="fullCenter">
                        <img width="180" src="http://www.masterspas.com/img/ms-logo.png" alt="masterspa_logo" border="0">
                    </td>
                </tr>
                </tbody></table>

                                    </td>
                                </tr>
                                </tbody></table>
                                <!-- End Nav -->
                            </td>
                        </tr>
                        </tbody></table>

                        <!-- Space -->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                            <tbody><tr>
                                <td width="100%" height="15"></td>
                            </tr>
                        </tbody></table>
                        <!-- End Space -->

                    </td>
                </tr>
                </tbody></table>
                <!-- End Header -->

                <!-- Main Content -->
                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                <tbody>
                <tr>
                    <td valign="top" width="100%">
                        <table class="mobile" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tbody>
                        <tr>
                            <td>
                                <!-- Space -->
                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                <tbody>
                                <tr>
                                    <td height="30" width="100%"></td>
                                </tr>
                                </tbody>
                                </table>
                                <!-- End Space -->

                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                <tbody>
                                <tr>
                                    <td width="100%">
                                        <!-- Centered Title -->
                                        <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                        <tbody>
                                        <tr>
                    <td style="text-align: center; font-family: Helvetica,Arial,sans-serif; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold; font-size: 26px;" class="fullCenter" valign="middle" width="100%">
                    <!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><!--<![endif]-->One more step to confirm your MasterSPA account...<!--[if !mso]><!--></span><!--<![endif]--></td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #000000; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
                        <p><!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><!--<![endif]-->Hello ' . $account->username . '!<!--[if !mso]><!--></span><!--<![endif]--></p>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><br></span></div>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;">Welcome to MasterSpas.  Before you can use this system you will need to wait for the administrator to confirm your account.</span></div>
                        <div><br></div>
                    </td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100">
                        <tbody>
                        <tr>
                            <td style="border-bottom: 1px solid #e5e5e5;" align="center" height="1" width="100"></td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="15" width="100%"></td>
                                        </tr>
                                        </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
                </table>
                <!-- End Main Content -->

                </body></html>';

        return $mailBody;
    }

    private function generateResetPasswordMail($accountModel)
    {
        $mailBody = '';

        $userId         = Yii::$app->getSecurity()->hashData($accountModel->id, 'userId');
        $expirationTime = Yii::$app->getSecurity()->hashData(
            Yii::$app->formatter->asDatetime('now +1 hour'),
            'expirationTime'
        );

        $resetLink  = Url::canonical() . '?u=' . $userId . '&t=' . $expirationTime;
        $mailBody = '<html><head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
                    <title>Your MasterSPA Account Confirmation Link</title>
                    <style type="text/css">
                        div, p, a, li, td { -webkit-text-size-adjust:none; }
                        *{-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}
                        .ReadMsgBody {width: 100%; background-color: #ffffff;}
                        .ExternalClass {width: 100%; background-color: #ffffff;}
                        body {width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
                        html{width: 100%; background-color: #ffffff;}
                        p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important;}
                        .image180 img {width: 180px; height: auto;}
                        .image280 img {width: 280px; height: auto;}
                        .logo img {width: 180px; height: auto;}
                        .image215 img {width: 215px; height: auto;}
                        .image600 img {width: 600px; height: auto;}
                        .image400 img {width: 400px; height: auto;}
                        .image300 img {width: 300px; height: auto;}
                        a:link {text-decoration:none;}
                        a:hover {text-decoration:none;}
                        a:active {text-decoration:none;}
                        a:visited {text-decoration:none;}
                    </style>
                    <!-- @media only screen and (max-width: 640px) {*/-->
                    <style type="text/css">
                    @media only screen and (max-width: 640px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .fontSmall {font-size: 55px!important; line-height: 62px!important; letter-spacing: 8px!important;}
                        .h80 {height: 90px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 40px!important;}
                        table[class=scale25] {width: 23%!important;}
                        table[class=scale25Right] {width: 23%!important;}
                        table[class=scale33] {width: 30%!important;}
                        table[class=scale33Right] {width: 30%!important;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        table[class=w5] {width: 3%!important; }
                        table[class=w10] {width: 5%!important; }
                        .pad0 {padding: 0px!important;}
                        .image600 img {width: 100%!important;}
                        .image180 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                    <!--@media only screen and (max-width: 479px) {-->
                    <style type="text/css">
                    @media only screen and (max-width: 479px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .buttonScale {text-align: center!important; float: none!important;}
                        .fontSmall {font-size: 42px!important; line-height: 48px!important; letter-spacing: 5px!important;}
                        .fontSmall2 {font-size: 34px!important; line-height: 38px!important; letter-spacing: 2px!important;}
                        .h80 {height: 80px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 30px!important;}
                        table[class=scale25] {width: 100%!important; clear: both;}
                        table[class=scale25Right] {width: 100%!important; clear: both;}
                        table[class=scale33] {width: 100%!important; clear: both;}
                        table[class=scale33Right] {width: 100%!important; clear: both;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        .h40 {height: 40px!important;}
                        .pad0 {padding: 0px!important;}
                        .image180 img {width: 100%!important;}
                        .image600 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                </head>
                <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">
                <!-- Border -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: #0088cc;">
                    <tbody><tr>
                        <td width="100%" height="2" valign="top">
                        </td>
                    </tr>
                </tbody></table>
                <!--End Border -->

                <!-- Header  -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: rgb(35, 40, 43);">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #eeeeee;" class="headerBG">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                        <tbody><tr>
                            <td width="100%">
                                <!-- Start Nav -->
                                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                <tbody><tr>
                                    <td width="100%" height="25"></td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="middle" class="logo">

                <!-- Logo -->
                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center;" class="fullCenter">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #ffffff; border-width: 5px; border-color: #ffffff; border-radius:10px; padding:5px;" height="60" valign="middle" width="100%" class="fullCenter">
                        <img width="180" src="http://www.masterspas.com/img/ms-logo.png" alt="masterspa_logo" border="0">
                    </td>
                </tr>
                </tbody></table>

                                    </td>
                                </tr>
                                </tbody></table>
                                <!-- End Nav -->
                            </td>
                        </tr>
                        </tbody></table>

                        <!-- Space -->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                            <tbody><tr>
                                <td width="100%" height="15"></td>
                            </tr>
                        </tbody></table>
                        <!-- End Space -->

                    </td>
                </tr>
                </tbody></table>
                <!-- End Header -->

                <!-- Main Content -->
                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                <tbody>
                <tr>
                    <td valign="top" width="100%">
                        <table class="mobile" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tbody>
                        <tr>
                            <td>
                                <!-- Space -->
                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                <tbody>
                                <tr>
                                    <td height="30" width="100%"></td>
                                </tr>
                                </tbody>
                                </table>
                                <!-- End Space -->

                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                <tbody>
                                <tr>
                                    <td width="100%">
                                        <!-- Centered Title -->
                                        <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                        <tbody>
                                        <tr>
                    <td style="text-align: center; font-family: Helvetica,Arial,sans-serif; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold; font-size: 26px;" class="fullCenter" valign="middle" width="100%">
                    <!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><!--<![endif]-->RESET PASSWORD<!--[if !mso]><!--></span><!--<![endif]--></td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #000000; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
                        <p><!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><!--<![endif]-->Hello ' . $accountModel->username . '!<!--[if !mso]><!--></span><!--<![endif]--></p>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><br></span></div>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;">Click the button below to change your password.</span></div>
                        <div><br></div>

                        <!-- New Confirm Button -->
                        <div><a href="' . $resetLink . '" style="background-color: rgb(0, 136, 204); border: 1px solid rgb(30, 54, 80); border-radius: 4px; color: rgb(255, 255, 255); display: inline-block; font-family: sans-serif; font-size: 16px; font-weight: bold; line-height: 40px; text-align: center; text-decoration: none; width: 200px;">Confirm my Account</a></div>
                        <!-- End New Button -->
                        <div><br><br></div>

                        <div><span style="font-family: "Open Sans", Helvetica; font-size:80%; font-weight: normal;">If the button above does not work, copy and paste this link into your browser:<br> <a href="#">' . $resetLink . '</a></span></div>
                        <div><br></div>
                    </td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100">
                        <tbody>
                        <tr>
                            <td style="border-bottom: 1px solid #e5e5e5;" align="center" height="1" width="100"></td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="15" width="100%"></td>
                                        </tr>
                                        </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
                </table>
                <!-- End Main Content -->

                </body></html>';

        return $mailBody;
    }
}

?>
