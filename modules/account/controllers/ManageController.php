<?php

namespace app\modules\account\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\User;
use app\models\UserInfo;
use app\components\helpers\Users;
use app\components\helpers\Data;

class ManageController extends \yii\web\Controller
{
    public $layout = '/commonLayout';
    public $route_nav;
    public $viewPath = 'app/modules/account/views';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // Pages that are included in the rule set
                'only'  => ['index', 'register', 'edit', 'delete', 'profile', 'changepassword', 'cpass'],
                'rules' => [
                    [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['index', 'register', 'delete', 'cpass'],
                        'roles'     => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $identity = Users::initUser();
                            return $identity->user_type == 'admin';
                        },
                    ],
                    [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['profile'],
                        'roles'     => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $identity = Users::initUser();
                            return $identity->user_type == 'user';
                        },
                    ],
                    [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['edit', 'changepassword'],
                        'roles'     => ['@'],
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    // Action when user is denied from accessing a page
                    if (Yii::$app->user->isGuest) {
                        $this->goHome();
                    } else {
                        $this->redirect(['/dashboard']);
                    }
                }
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init()
    {
        if(Yii::$app->user->isGuest) {
            $url = '/login';
        } else {
            $url = '/dashboard';
        }
    }

    public function actionIndex()
    {
        $model = new User;

        $records = $model->getRecords();

        return $this->render('list', [
            'records' => $records
        ]);
    }

    public function actionRegister()
    {
        $model = new User;
        $model->scenario = 'registerAccount';
        $model2 = new UserInfo;

        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                if ( $model->saveRecord(Yii::$app->request->post('User'), Yii::$app->request->post('UserInfo'))) {
                    return $this->redirect('/account/manage');
                } else {
                    Yii::$app->session->setFlash('error', 'There was an error while saving account. Please try again later.');
                }

                return $this->refresh();
            } else {
                $errors = $model->getErrors();
                foreach ($errors as $key => $error) {
                    $save = false;
                    $model->addError($key, $error[0]);
                }
            }
        }

        return $this->render('editor', [
            'model' => $model,
            'model2' => $model2
        ]);
    }

    public function actionEdit($id)
    {
        $model = User::find()->where([
            'id'        => $id,
            'user_type' => 'user',
            'status'    => ['active', 'unconfirmed']
        ])->one();

        $identity = Users::initUser();

        if($identity->user_type === 'user' && $identity->id != $id) {
            return $this->redirect('/profile');
        }

        if(empty($model)) {
            return $this->redirect('/account/manage');
        }

        $model2 = UserInfo::find()->where([
            'user_id'   => $id,
        ])->one();

        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                $record = User::findOne($id);
                $user = Yii::$app->request->post('User');
                $userInfo = Yii::$app->request->post('UserInfo');

                $save = false;

                if($user['username'] !== $record->username) {
                    $count = count(Data::findRecords(new User, null, ['username' => $user['username']], 'all'));

                    if($count != 0) {
                        $model->addError('username', 'Username already taken');
                        $save = false;
                    } else {
                        $save = true;
                    }
                } else {
                    $save = true;
                }

                if($user['email_address'] !== $record->email_address) {
                    $count = count(Data::findRecords(new User, null, ['email_address' => $user['email_address']], 'all'));

                    if($count != 0) {
                        $model->addError('email_address', 'Email Address already taken');
                        $save = false;
                    } else {
                        $save = true;
                    }
                } else {
                    $save = ($save) ? true : false;
                }

                if($save == true) {
                    if ( $model->saveRecord($user, $userInfo, $id)) {
                        return $this->redirect('/account/manage');
                    } else {
                        Yii::$app->session->setFlash('error', 'There was an error while saving account. Please try again later.');
                    }
                    return $this->refresh();
                }

            } else {
                $errors = $model->getErrors();
                foreach ($errors as $key => $error) {
                    $save = false;
                    $model->addError($key, $error[0]);
                }
            }
        }

        return $this->render('editor', [
            'model' => $model,
            'model2' => $model2,
            'id' => $id
        ]);

    }

    public function actionActivate($id)
    {
        $model = User::findOne($id);

        $model->status = 'active';

        if($model->save()) {
            $mailBody = $this->generateActivateMailBody($model);
            if (Yii::$app->mailer->compose()
                                    ->setCharset('UTF-8')
                                    ->setFrom([Yii::$app->params['adminEmail'] => 'Master Spas'])
                                    ->setTo($model->email_address)
                                    ->setSubject('Account Activated')
                                    ->setHtmlBody($mailBody)
                                    ->send()) {
                return '/account/manage';
            } else {
                return false;
            }
        }

        return false;
    }

    public function actionDelete($id)
    {
        $model = User::findOne($id);

        $model->status = 'inactive';

        if($model->save()) {
            return '/account/manage';
        }

        return false;
    }

    public function actionProfile()
    {
        $identity = Users::initUser();

        $model = User::find()->where([
            'id'        => $identity->id,
            'user_type' => 'user',
            'status'    => 'active'
        ])->one();

        $model2 = UserInfo::find()->where([
            'user_id'   => $identity->id,
        ])->one();

        return $this->render('profile', [
            'model' => $model,
            'model2' => $model2
        ]);

    }

    public function actionCpass($id)
    {
        $model = new User;
        $model = User::findOne($id);

        if(empty($model)) {
            return $this->redirect('/account/manage');
        }

        $model->password='';
        $model->scenario = 'changePassAdmin';


        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                $user = Yii::$app->request->post('User');
                 if($model->changePassword($id, $user['password'])) {
                    Yii::$app->session->setFlash('success', 'Password updated');
                } else {
                    Yii::$app->session->setFlash('error', 'There was an error while saving account. Please try again later.');
                }
                return $this->refresh();
            } else {
                $errors = $model->getErrors();
                foreach ($errors as $key => $error) {
                    $save = false;
                    $model->addError($key, $error[0]);
                }
            }
        }

        return $this->render('changepassword', [
            'model' => $model,
            'admin' => 1
        ]);

    }

    public function actionChangepassword()
    {
        $model = new User;
        $identity = Users::initUser();

        $model = User::findOne($identity->id);
        $model->password='';
        $model->scenario = 'changePass';

         if ($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                $user = Yii::$app->request->post('User');
                if (User::validatePassword($user['oldPassword'], $identity->password)) {
                    if($model->changePassword($identity->id, $user['password'])) {
                        Yii::$app->session->setFlash('success', 'Password updated');
                    } else {
                        Yii::$app->session->setFlash('error', 'There was an error while saving account. Please try again later.');
                    }
                    return $this->refresh();
                } else {
                    $model->addError('oldPassword', 'Old Password is incorrect');
                }
            }  else {
                $errors = $model->getErrors();
                foreach ($errors as $key => $error) {
                    $save = false;
                    $model->addError($key, $error[0]);
                }
            }
         }

        return $this->render('changepassword', [
            'model' => $model
        ]);
    }

    private function generateActivateMailBody($account)
    {
        $mailBody = '';
        $userId = Yii::$app->getSecurity()->hashData($account->username, 'account');
        $mailBody .= '<html><head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
                    <title>Your MasterSPA Account Confirmation Link</title>
                    <style type="text/css">
                        div, p, a, li, td { -webkit-text-size-adjust:none; }
                        *{-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}
                        .ReadMsgBody {width: 100%; background-color: #ffffff;}
                        .ExternalClass {width: 100%; background-color: #ffffff;}
                        body {width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
                        html{width: 100%; background-color: #ffffff;}
                        p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important;}
                        .image180 img {width: 180px; height: auto;}
                        .image280 img {width: 280px; height: auto;}
                        .logo img {width: 180px; height: auto;}
                        .image215 img {width: 215px; height: auto;}
                        .image600 img {width: 600px; height: auto;}
                        .image400 img {width: 400px; height: auto;}
                        .image300 img {width: 300px; height: auto;}
                        a:link {text-decoration:none;}
                        a:hover {text-decoration:none;}
                        a:active {text-decoration:none;}
                        a:visited {text-decoration:none;}
                    </style>
                    <!-- @media only screen and (max-width: 640px) {*/-->
                    <style type="text/css">
                    @media only screen and (max-width: 640px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .fontSmall {font-size: 55px!important; line-height: 62px!important; letter-spacing: 8px!important;}
                        .h80 {height: 90px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 40px!important;}
                        table[class=scale25] {width: 23%!important;}
                        table[class=scale25Right] {width: 23%!important;}
                        table[class=scale33] {width: 30%!important;}
                        table[class=scale33Right] {width: 30%!important;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        table[class=w5] {width: 3%!important; }
                        table[class=w10] {width: 5%!important; }
                        .pad0 {padding: 0px!important;}
                        .image600 img {width: 100%!important;}
                        .image180 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                    <!--@media only screen and (max-width: 479px) {-->
                    <style type="text/css">
                    @media only screen and (max-width: 479px){
                        body{width:auto!important;}
                        table[class=full] {width: 100%!important; clear: both; }
                        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                        .buttonScale {text-align: center!important; float: none!important;}
                        .fontSmall {font-size: 42px!important; line-height: 48px!important; letter-spacing: 5px!important;}
                        .fontSmall2 {font-size: 34px!important; line-height: 38px!important; letter-spacing: 2px!important;}
                        .h80 {height: 80px!important;}
                        .h50 {height: 50px!important;}
                        .h30 {height: 30px!important;}
                        table[class=scale25] {width: 100%!important; clear: both;}
                        table[class=scale25Right] {width: 100%!important; clear: both;}
                        table[class=scale33] {width: 100%!important; clear: both;}
                        table[class=scale33Right] {width: 100%!important; clear: both;}
                        .image143 img {width: 100%!important; height: auto;}
                        .image195 img {width: 100%!important; height: auto;}
                        .image400 img {width: 100%!important;}
                        .image300 img {width: 100%!important;}
                        .h40 {height: 40px!important;}
                        .pad0 {padding: 0px!important;}
                        .image180 img {width: 100%!important;}
                        .image600 img {width: 100%!important;}
                        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
                        .h15 {width: 100%!important; height: 15px!important;}
                        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    }
                    </style>
                </head>
                <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">
                <!-- Border -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: #0088cc;">
                    <tbody><tr>
                        <td width="100%" height="2" valign="top">
                        </td>
                    </tr>
                </tbody></table>
                <!--End Border -->

                <!-- Header  -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="background-color: rgb(35, 40, 43);">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #eeeeee;" class="headerBG">
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                        <tbody><tr>
                            <td width="100%">
                                <!-- Start Nav -->
                                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                <tbody><tr>
                                    <td width="100%" height="25"></td>
                                </tr>
                                <tr>
                                    <td width="100%" valign="middle" class="logo">

                <!-- Logo -->
                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center;" class="fullCenter">
                <tbody><tr>
                    <td style="-webkit-background-size: cover; background-color: #ffffff; border-width: 5px; border-color: #ffffff; border-radius:10px; padding:5px;" height="60" valign="middle" width="100%" class="fullCenter">
                        <img width="180" src="http://www.masterspas.com/img/ms-logo.png" alt="masterspa_logo" border="0">
                    </td>
                </tr>
                </tbody></table>

                                    </td>
                                </tr>
                                </tbody></table>
                                <!-- End Nav -->
                            </td>
                        </tr>
                        </tbody></table>

                        <!-- Space -->
                        <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                            <tbody><tr>
                                <td width="100%" height="15"></td>
                            </tr>
                        </tbody></table>
                        <!-- End Space -->

                    </td>
                </tr>
                </tbody></table>
                <!-- End Header -->

                <!-- Main Content -->
                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                <tbody>
                <tr>
                    <td valign="top" width="100%">
                        <table class="mobile" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                        <tbody>
                        <tr>
                            <td>
                                <!-- Space -->
                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                <tbody>
                                <tr>
                                    <td height="30" width="100%"></td>
                                </tr>
                                </tbody>
                                </table>
                                <!-- End Space -->

                                <table class="full" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                <tbody>
                                <tr>
                                    <td width="100%">
                                        <!-- Centered Title -->
                                        <table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter" border="0" cellpadding="0" cellspacing="0" align="center" width="600">
                                        <tbody>
                                        <tr>
                    <td style="text-align: center; font-family: Helvetica,Arial,sans-serif; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold; font-size: 26px;" class="fullCenter" valign="middle" width="100%">
                    <!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><!--<![endif]-->Congratulations !!!<!--[if !mso]><!--></span><!--<![endif]--></td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #000000; line-height: 22px;" class="fullCenter" valign="middle" width="100%">
                        <p><!--[if !mso]><!--><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><!--<![endif]-->Hello ' . $account->username . '!<!--[if !mso]><!--></span><!--<![endif]--></p>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;"><br></span></div>
                        <div><span style="font-family: "Open Sans", Helvetica; font-weight: normal;">Welcome to MasterSpas.  Your account has been activated.</span></div>
                        <div><br></div>
                    </td>
                </tr>
                <tr>
                    <td height="20" width="100%"></td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100">
                        <tbody>
                        <tr>
                            <td style="border-bottom: 1px solid #e5e5e5;" align="center" height="1" width="100"></td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="15" width="100%"></td>
                                        </tr>
                                        </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
                </table>
                <!-- End Main Content -->

                </body></html>';

        return $mailBody;
    }
}

?>
