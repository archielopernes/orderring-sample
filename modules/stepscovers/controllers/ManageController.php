<?php

namespace app\modules\stepscovers\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\StepsCovers;
use app\models\Order;
use app\components\helpers\Users;
use app\components\helpers\Data;
use yii\db\Query;

class ManageController extends \yii\web\Controller
{
    public $layout = '/commonLayout';
    public $route_nav;
    public $viewPath = 'app/modules/stepscovers/views';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // Pages that are included in the rule set
                'only'  => ['index'],
                'rules' => [
                    [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['index'],
                        'roles'     => ['@'],
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    // Action when user is denied from accessing a page
                    if (Yii::$app->user->isGuest) {
                        $this->goHome();
                    } else {
                        $this->redirect(['/dashboard']);
                    }
                }
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init()
    {
        if(Yii::$app->user->isGuest) {
            $url = '/admin/login';
        } else {
            $url = '/admin/dashboard';
        }
    }

    public function actionList()
    {
        $model = new StepsCovers;

        $records = $model->getRecords();

        return $this->render('list', [
            'records'     => $records
        ]);
    }

    public function actionAdd()
    {
        $model = new StepsCovers;

        if(Yii::$app->request->isPost) {
            if($model->saveRecord(Yii::$app->request->post('StepsCovers'))) {
                return $this->redirect('/stepscovers/manage');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error while processing your order. Please try again later.');
            }
        }

        return $this->render('add', [
            'model'     => $model
        ]);
    }

     public function actionEdit($id)
    {
        $model = new StepsCovers;
        $record = StepsCovers::findOne($id);

         if(empty($record)) {
             return $this->redirect('/stepscovers/manage');
         }

        if(Yii::$app->request->isPost) {
            if($model->saveRecord(Yii::$app->request->post('StepsCovers'), $id)) {
                return $this->redirect('/stepscovers/manage');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error while processing your order. Please try again later.');
            }
        }

        return $this->render('add', [
            'model'     => $record
        ]);
    }

    public function actionDelete($id)
    {
        $record = StepsCovers::findOne($id);

        if(Yii::$app->request->isPost && !empty($record)) {
            $record->status = 'deleted';
            $record->save();
        }

        return '/stepscovers/manage';
    }


}

?>
