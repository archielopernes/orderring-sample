<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\BaseUrl;
    use yii\helpers\Url;
    use app\components\helpers\Users;
    use app\assets\DatatablesAsset;
    use app\assets\MsgboxAsset;
    DatatablesAsset::register($this);
    MsgboxAsset::register($this);

    $this->title = 'Steps / Covers List';
?>

<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered" id="steps-covers-list">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">NAME</th>
                        <th class="text-center">DESCRIPTION</th>
                        <th class="text-center">TYPE</th>
                        <th class="text-center">PRODUCT TYPE</th>
                        <th class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($records as $value) : ?>
                    <tr>
                        <td class="text-center"><?= $value['id'] ?></td>
                        <td class="text-center"><?= $value['name'] ?></td>
                        <td class="text-center"><?= $value['description'] ?></td>
                        <td class="text-center"><?= $value['type'] ?></td>
                        <td class="text-center"><?= $value['q_type'] === 'swim_spa' ? 'Swim Spa' : 'Hot Tub' ?></td>
                        <td class="text-center">
                            <ul class="list-inline">
                                <li>
                                    <a href="/stepscovers/manage/edit/<?= $value['id'] ?>">
                                        <button type="button" class="btn btn-primary">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </button>
                                    </a>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-danger delete-steps-covers-btn" id="<?= $value['id'] ?>">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </button>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
         <div class="panel-footer text-right">
             <a href="/stepscovers/manage/add" class="btn btn-primary">Add Steps / Covers</a>
        </div>
    </div>
</div>
