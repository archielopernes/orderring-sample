<?php
    use yii\bootstrap\ActiveForm;
    use app\assets\MsgboxAsset;
    MsgboxAsset::register($this);

    $this->title = 'Add Step / Cover';
?>

<br>
<?php $form = ActiveForm::begin([
        'id'        => 'editor-step-cover-form',
        'method'    => 'post',
        'layout'    => 'horizontal'
    ]);
?>

<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">
            <br>
            <!-- Flash Messages -->
            <?php if (Yii::$app->session->hasFlash('error')) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Error!</strong> <?= Yii::$app->session->getFlash('error'); ?>
                </div>
            <?php } ?>
            <br>
            <?= $form->field($model, 'name')->textInput() ?>
            <?= $form->field($model, 'description')->textInput() ?>
            <?= $form->field($model, 'type')->dropDownList(['steps' => 'Steps', 'covers' => 'Covers']) ?>
            <?= $form->field($model, 'q_type')->dropDownList(['swim_spa' => 'Swim Spa', 'hot_tub' => 'Hot Tub']) ?>
        </div>
        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-info">Save Step / Cover</button>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
