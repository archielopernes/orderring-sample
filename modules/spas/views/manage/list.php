<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\BaseUrl;
    use yii\helpers\Url;
    use app\components\helpers\Users;
    use app\assets\DatatablesAsset;
    use app\assets\InventoryAsset;
    use app\assets\MsgboxAsset;
    DatatablesAsset::register($this);
    InventoryAsset::register($this);
    MsgboxAsset::register($this);

    $this->title = 'Spa List';
?>

<!-- Modal -->
<div class="modal fade" id="importCsvModal" tabindex="-1" role="dialog" aria-labelledby="importCsvModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Import CSV</h4>
      </div>
      <div class="modal-body">
        <div class="loader center-block"></div>
        <p id="error-msg" class="bg-danger"></p>
        <div>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'csv-form']]) ?>
                <div class="form-group">
                    <label class="col-sm-2 control-label">CSV File</label>
                    <div class="col-sm-10">
                        <button type="button" class="btn btn-info" id="csv-btn">Upload CSV File</button>&nbsp;<span id="csvFileName"></span>
                        <?= $form->field($model, 'csvFile')->fileInput(['class' => 'hidden csv-file'])->label(''); ?>
                    </div>
                </div>
            <?php ActiveForm::end() ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="csv-submit">Import CSV</button>
      </div>
    </div>
  </div>
</div>

<br>

<!-- Flash Messages -->
<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> <?= Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-warning alert-dismissible" role="warning">
        <button type="button" class="close" data-dismiss="warning" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Warning!</strong> <?= Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } ?>


<div class="content">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $this->title; ?></h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered" id="inventory-list">
                <thead>
                    <tr>
                        <th class="text-center">SERIAL NO.</th>
                        <th class="text-center">MODEL</th>
                        <th class="text-center">COLOR</th>
                        <th class="text-center">SKIRT</th>
                        <th class="text-center">LEDS</th>
                        <th class="text-center">FULL FOAM</th>
                        <th class="text-center">STEREO</th>
                        <th class="text-center">SPECIAL</th>
                        <th class="text-center">FRAME</th>
                        <th class="text-center">JETS</th>
                        <th class="text-center">EXT</th>
                        <th class="text-center">DATE COMPLETED</th>
                        <th class="text-center">STATUS</th>
                        <th class="text-center">TYPE</th>
                        <?php // if(Users::initUser()->user_type != 'admin') { ?>
                            <th></th>
                        <?php // } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($records as $value) : ?>
                    <tr>
                        <td class="text-center"><?= $value['serial_no'] ?></td>
                        <td class="text-center"><?= $value['model'] ?></td>
                        <td class="text-center"><?= $value['color'] ?></td>
                        <td class="text-center"><?= $value['skirt'] ?></td>
                        <td class="text-center"><?= $value['leds'] ?></td>
                        <td class="text-center"><?= $value['full_foam'] ?></td>
                        <td class="text-center"><?= $value['stereo'] ?></td>
                        <td class="text-center"><?= $value['special'] ?></td>
                        <td class="text-center"><?= $value['frame'] ?></td>
                        <td class="text-center"><?= $value['jets'] ?></td>
                        <td class="text-center"><?= $value['ext'] ?></td>
                        <td class="text-center"><?= $value['date_completed'] ?></td>
                        <td class="text-center"><?= ucfirst($value['status']) ?></td>
                        <td class="text-center">
                            <?php if (substr($value['serial_no'], 0, 1) === '1' || substr($value['serial_no'], 0, 1) === 'R') {
                                echo 'Hot Tub';
                            } else {
                                echo 'Swim Spa';
                            } ?>
                        </td>
                        <td>
                            <?php if(Users::initUser()->user_type != 'admin') { ?>
                                <a href="/order/manage/<?= $value['serial_no']; ?>">
                                    <button class="btn btn-primary">Buy Now</button>
                                </a>
                            <?php } else {  ?>
                                <ul class="list-inline text-center">
                                    <li style="margin-bottom: 5px;">
                                    <?php if ($value['ignore_flag'] == 1) { ?>
                                        <button class="btn btn-primary ignore-flag" value="0" data-toggle="tooltip" data-placement="top" title="Remove Ignore on import" id="<?= $value['serial_no'] ?>">
                                            <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
                                    <?php } else { ?>
                                        <button class="btn btn-primary ignore-flag" value="1" data-toggle="tooltip" data-placement="top" title="Ignore on import" id="<?= $value['serial_no'] ?>">
                                            <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
                                    <?php } ?>
                                    </button>
                                    </li>
                                    <li style="margin-bottom: 5px;">
                                        <button class="btn btn-danger delete-spa" data-toggle="tooltip" data-placement="top" title="Delete Spa"
                                                id="<?= $value['serial_no'] ?>">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </li>
                                </ul>
                            <?php } ?>
                        </td>

                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php if(Users::initUser()->user_type == 'admin') { ?>
            <div class="panel-footer">
                <ul class="list-inline text-right">
                    <li>
                        <a href="/spas/manage/spaexport" class="btn btn-warning">Export CSV</a>
                    </li>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#importCsvModal">Import CSV</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-primary" disabled>Add Stock</button>
                    </li>
                </ul>
            </div>
        <?php } ?>
    </div>
</div>
