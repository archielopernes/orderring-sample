<?php

namespace app\modules\spas\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Product;
use app\components\helpers\Users;
use app\components\helpers\Data;
use app\models\UploadForm;
use yii\web\UploadedFile;
use yii\db\Query;

class ManageController extends \yii\web\Controller
{
    public $layout = '/commonLayout';
    public $route_nav;
    public $viewPath = 'app/modules/spas/views';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // Pages that are included in the rule set
                'only'  => ['index'],
                'rules' => [
                    [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['index'],
                        'roles'     => ['@'],
//                        'matchCallback' => function ($rule, $action) {
//                            $identity = Users::initUser();
//                            return $identity->user_type == 'admin';
//                        },
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    // Action when user is denied from accessing a page
                    if (Yii::$app->user->isGuest) {
                        $this->goHome();
                    } else {
                        $this->redirect(['/dashboard']);
                    }
                }
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init()
    {
        if(Yii::$app->user->isGuest) {
            $url = '/admin/login';
        } else {
            $url = '/admin/dashboard';
        }
    }

    public function actionIndex()
    {
        $identity = Users::initUser();
        $model = new Product;
        $model->scenario = 'importCsv';

        if ($identity->user_type == 'admin') {
            $records = Product::find()->all();
        } else {
            $records = Product::find()->where(['status' => 'available'])->all();
        }

         if (Yii::$app->request->isPost) {
            $model->csvFile = UploadedFile::getInstance($model, 'csvFile');
            if ($model->upload()) {
                $flag = $model->uploadData($model->csvFile);

                if (empty($flag)) {
                    Yii::$app->session->setFlash('success', 'You have successfully imported the CSV file.');
                } else {
                    Yii::$app->session->setFlash('error', 'Duplicate Entries : ' . implode(", ", $flag));
                }
            } else {
                Yii::$app->session->setFlash('error', 'Unable to upload CSV File.');
            }

            return $this->refresh();
         }

        return $this->render('list', [
            'records'   => $records,
            'model'     => $model
        ]);
    }

    public function actionFlag($id, $value)
    {
        $model = new Product;
        $toUpdate = ['ignore_flag' => $value];

        if($model->updateColumns($id, $toUpdate)) {
            return '/spas/manage';
        }

        return false;
    }

    public function actionDelete($id)
    {
        $model = new Product;

        if($model->deleteProduct($id)) {
            return '/spas/manage';
        }

        return false;
    }

    public function actionSpaexport()
    {
		header('Content-Type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=spas.csv');

        $spaModel = new Product;
        $items = $spaModel->getProducts();

        $flag = 0;
		$output = fopen('php://output', 'w');
		$columnnames = array('SERIAL #','MODEL','COLOR','SKIRT','LED','FF','ST','Spec','SSJ',
                             'FRM','EXT','COMPLETED DATE', 'OPTIONSTRING', 'REVISION');

		fputcsv($output, $columnnames);
		foreach ($items as $fields) {
            $date = date_create($fields['date_completed']);
            $fields['date_completed'] = date_format($date, 'Y/m/d');
			fputcsv($output, $fields);
            $flag++;
		}
		fclose($output);

        $session = Yii::$app->session;
        $session->open();
        $session->set('flag_count_products', $flag);
        $session->close();
    }
}

?>
