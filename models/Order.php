<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\base\Exception;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $serial_no
 * @property integer $user_id
 * @property string $status
 * @property string $order_status
 * @property integer $steps
 * @property integer $covers
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product $serialNo
 * @property User $user
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serial_no', 'user_id', 'status'], 'required'],
            [['user_id', 'steps', 'covers'], 'integer'],
            [['status', 'order_status'], 'string'],
            [['serial_no'], 'string', 'max' => 50],
            [['created_at', 'updated_at'], 'string', 'max' => 255],
            [['serial_no'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['serial_no' => 'serial_no']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'serial_no' => 'Serial No',
            'user_id' => 'User ID',
            'status' => 'Status',
            'order_status' => 'Order Status',
            'steps' => 'Steps',
            'covers' => 'Covers',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getSerialNo()
    {
        return $this->hasOne(Product::className(), ['serial_no' => 'serial_no']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getRecords($condition)
    {
        $record = self::find()->where($condition)->asArray()->orderBy('id DESC')->all();
        return $record;

    }

    public function saveOrder($serialNo, $steps, $covers, $customer)
    {
        $connection = Yii::$app->db;
        $transaction =  $connection->beginTransaction();

        try {
            $order = new Order;

            $order->serial_no = $serialNo;
            $order->user_id = $customer;
            $order->status = 'active';
            $order->created_at = $order->updated_at = date('Y/m/d H:i:s');
            $order->steps = intval($steps);
            $order->covers = intval($covers);

            if ($order->save()) {

                $product = Product::findOne($serialNo);
                $product->status = 'sold';

                if($product->save(false)) {
                    $transaction->commit();
                    return true;
                } else {
                    $transaction->rollBack();
                    return false;
                }
            } else {
                $transaction->rollBack();
                return false;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    public function getOrder($export, $userId=null)
    {
        $query = new Query();
        if ($export) {
            $query->select(["CONCAT(first_name, ' ', last_name) AS full_name", 'p.serial_no', 'u.customer_no', 'sc2.description as cover', 'sc.description as step', 'o.id', 'o.*', 'p.model', 'p.option_str', 'p.revision', 'p.order_sage_no'])
                ->from('orders o')
                ->leftJoin('product p', 'p.serial_no = o.serial_no')
                ->leftJoin('user u', 'u.id = o.user_id and u.status = "active"')
                ->leftJoin('user_info ui', 'u.id = ui.user_id')
                ->leftJoin('steps_covers sc', 'sc.id = o.steps')
                ->leftJoin('steps_covers sc2', 'sc2.id = o.covers');
        } else {
            $query->select(["CONCAT(first_name, ' ', last_name) AS full_name", 'p.serial_no', 'u.customer_no', 'sc2.name as cover', 'sc.name as step', 'o.*', 'p.model', 'p.order_sage_no'])
                ->from('orders o')
                ->leftJoin('product p', 'p.serial_no = o.serial_no')
                ->leftJoin('user u', 'u.id = o.user_id and u.status = "active"')
                ->leftJoin('user_info ui', 'u.id = ui.user_id')
                ->leftJoin('steps_covers sc', 'sc.id = o.steps')
                ->leftJoin('steps_covers sc2', 'sc2.id = o.covers');
        }

        if (!empty($userId)) {
            $query->params([':user_id' => $userId]);
            $query->where('o.user_id = :user_id and o.status="active"');
        } else {
            $query->where('o.status<>"deleted"');
        }

        $command = $query->createCommand(Yii::$app->db);
        $rows = $command->queryAll();

        return $rows;
    }

    public function updateColumns($id, $toUpdate)
    {
        $record = self::findOne($id);

        foreach($toUpdate as $key => $value) {
            $record->$key = $value;
        }

        return $record->save();
    }

    public function cancelOrder($orderId)
    {
        $connection = Yii::$app->db;
        $transaction =  $connection->beginTransaction();

        try {
            $order = self::findOne($orderId);
            $order->status = 'cancelled';

            if ($order->save()) {
                $product = Product::findOne($order->serial_no);
                $product->status = 'available';

                if($product->save()) {
                    $transaction->commit();
                    return true;
                } else {
                    $transaction->rollBack();
                    return false;
                }
            } else {
                $transaction->rollBack();
                return false;
            }

        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }
}
