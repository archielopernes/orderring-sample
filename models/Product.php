<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use app\components\helpers\Data;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product".
 *
 * @property string $serial_no
 * @property string $model
 * @property string $color
 * @property string $skirt
 * @property string $leds
 * @property string $full_foam
 * @property string $stereo
 * @property string $special
 * @property string $frame
 * @property string $jets
 * @property string $ext
 * @property string $date_completed
 * @property string $option_str
 * @property string $revision
 * @property string $order_sage_no
 * @property string $status
 * @property integer $type
 * @property integer $ignore_flag
 *
 * @property Orders[] $orders
 */
class Product extends \yii\db\ActiveRecord
{
    public $csvFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serial_no', 'model', 'color', 'skirt', 'date_completed', 'status'], 'required'],
            [['date_completed'], 'safe'],
            [['status'], 'string'],
            [['type', 'ignore_flag'], 'integer'],
            [['serial_no', 'stereo', 'special', 'frame', 'jets', 'ext', 'option_str', 'revision'], 'string', 'max' => 20],
            [['model'], 'string', 'max' => 255],
            [['order_sage_no'], 'string', 'max' => 200],
            [['color', 'skirt'], 'string', 'max' => 100],
            [['leds', 'full_foam'], 'string', 'max' => 50],
            [['csvFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'csv', 'on' => 'importCsv'],
            [['csvFile'], 'required', 'on' => 'importCsv']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'serial_no' => 'Serial No',
            'model' => 'Model',
            'color' => 'Color',
            'skirt' => 'Skirt',
            'leds' => 'Leds',
            'full_foam' => 'Full Foam',
            'stereo' => 'Stereo',
            'special' => 'Special',
            'frame' => 'Frame',
            'jets' => 'Jets',
            'ext' => 'Ext',
            'date_completed' => 'Date Completed',
            'option_str' => 'Option String',
            'revision' => 'Revision',
            'order_sage_no' => 'Order Sage Number',
            'status' => 'Status',
            'type' => 'Type',
            'ignore_flag' => 'Ignore Flag',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['serial_no' => 'serial_no']);
    }


     public function upload()
    {
        $dir = 'resources/csv';

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        if (file_exists($dir.'/csv' . '.' . $this->csvFile->extension)) {
            unlink($dir.'/csv' . '.' . $this->csvFile->extension);
//            mkdir($dir, 0777, true);
        }

        if($this->csvFile->saveAs($dir.'/csv' . '.' . $this->csvFile->extension)) {
            chmod($dir.'/csv' . '.' . $this->csvFile->extension, 0777);
            return true;
        }

        return false;
    }

    public function uploadData($file) {
        $duplicate_serial = [];
        $flag = null;
        $connection = Yii::$app->db;
        $root = getcwd();
        $saveDir = $root.'/resources/csv/csv.csv';
        $row = 1;
        $serial_nos_csv = [];
        if (($handle = fopen($saveDir, "r")) !== FALSE) {
           fgetcsv($handle);
           while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
               $serialNo = $data[0];
               array_push($serial_nos_csv, $serialNo);
               $record = self::findOne($serialNo);
               if(!empty($record)) {
                   array_push($duplicate_serial, $serialNo);
               }
            }
            fclose($handle);
        }

        $model = new Product;
        $serial_nos = self::find()
                        ->select('serial_no')
                        ->where([
                            'status'    => 'available',
                            'ignore_flag' => 0
                        ])
                        ->asArray()
                        ->all();

        $callBack = function($value) {
            return $value['serial_no'];
        };

        $arr = array_map($callBack, $serial_nos);
        $prod_to_delete = array_diff($arr, $serial_nos_csv);

        $sql = "LOAD DATA LOCAL INFILE '$saveDir' IGNORE
                INTO TABLE product
                FIELDS TERMINATED BY ','
                ENCLOSED BY '\"' ESCAPED BY '\"'
                LINES TERMINATED BY '\\r\\n'
                IGNORE 1 LINES
                (serial_no, model, color, skirt, leds, full_foam, stereo, special, jets, frame, ext, @date, option_str, revision, order_sage_no)
                SET date_completed= (
                    CASE
                        WHEN @date REGEXP '[0-9]{2}/[0-9]{2}/[0-9]{4}' THEN STR_TO_DATE(@date,'%m/%d/%Y')
                    END
                )";

        if(!empty($file)) {
            $command=$connection->createCommand($sql);
            $command->execute();
        }

        $deleted = self::deleteAll(['serial_no' => $prod_to_delete]);

        return $duplicate_serial;
    }

    public function getProducts()
    {
        $query = new Query;
        $query->select('serial_no, model, color, skirt, leds, full_foam, stereo, special, jets, frame, ext, date_completed, option_str, revision')
              ->from('product')
		      ->where('status = "available"');
        $command = $query->createCommand(Yii::$app->db);
        $rows = $command->queryAll();

        return $rows;
    }


    /*public function saveInfile($file)
    {
        $flag = null;
        $connection = Yii::$app->db;
        $root = getcwd();
        $saveDir = $root.'/resources/csv/csv.csv';

         $sql = "LOAD DATA INFILE '$saveDir' IGNORE
                INTO TABLE product
                FIELDS TERMINATED BY ',' ENCLOSED BY '\"' ESCAPED BY '\"'
                LINES TERMINATED BY '\\r\\n'
                IGNORE 1 LINES
                SET status='available', type = 1";

        $transaction =  $connection->beginTransaction();

        try {
            if(!empty($file)){
                $command=$connection->createCommand($sql);
                if($command->execute()){
                    $flag = true;
                    $transaction->commit();
                }
            }
        } catch (Exception $e) {
            $flag = $e->errorInfo[2];
            $transaction->rollBack();
        }


        return $flag;
    }*/

    public function updateColumns($id, $toUpdate)
    {
        $record = self::findOne($id);

        foreach($toUpdate as $key => $value) {
            $record->$key = $value;
        }

        return $record->save();
    }

    public function deleteProduct($serialNo)
    {
        $connection = Yii::$app->db;
        $transaction =  $connection->beginTransaction();

        try {
            $record = self::findOne($serialNo);

            if($record->status == 'sold') {
                $order = Order::findOne(['serial_no' => $serialNo]);

                if(!$order->delete()) {
                    var_dump($order->errors);
                    $transaction->rollBack();
                    return false;
                }
            }

            if($record->delete()) {
                $transaction->commit();
                return true;
            } else {
                var_dump($record->errors);
                $transaction->rollBack();
                return false;
            }

        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }
}
