<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\base\Exception;

/**
 * This is the model class for table "steps_covers".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property string $q_type
 * @property string $status
 */
class StepsCovers extends \yii\db\ActiveRecord
{
    public $steps;
    public $covers;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'steps_covers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['type', 'q_type', 'status', 'steps', 'covers'], 'string'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'type' => 'Type',
            'q_type' => 'Product Type',
            'status' => 'Status',
        ];
    }

    public function getRecords()
    {
        $query = new Query();

        $query->select(['*'])
            ->from('steps_covers')
            ->where('status = "active"');

        $command = $query->createCommand(Yii::$app->db);
        $rows = $command->queryAll();

        return $rows;
    }

    public function getData($type)
    {
        $query = new Query();

        $query->select('*')
            ->from('steps_covers')
            ->where('status = "active" and type = "' . $type . '"');

        $command = $query->createCommand(Yii::$app->db);
        $rows = $command->queryAll();

        return $rows;
    }

    public function saveRecord($attributes, $id=null)
    {
        if (empty($id)) {
            $model = new StepsCovers;
        } else {
            $model = self::findOne($id);
        }

        $model->name = $attributes['name'];
        $model->description = $attributes['description'];
        $model->type = $attributes['type'];
        $model->q_type = $attributes['q_type'];
        $model->status = 'active';

        return $model->save();
    }
}
