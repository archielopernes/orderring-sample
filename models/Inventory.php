<?php

namespace app\models;

use Yii;
use yii\base\Exception;


/**
 * This is the model class for table "inventory".
 *
 * @property string $serial_no
 * @property string $model
 * @property string $color
 * @property string $skirt
 * @property string $leds
 * @property string $full_foam
 * @property string $stereo
 * @property string $special
 * @property string $frame
 * @property string $jets
 * @property string $ext
 * @property string $date_completed
 */
class Inventory extends \yii\db\ActiveRecord
{
    public $csvFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serial_no', 'model', 'color', 'skirt', 'date_completed'], 'required'],
            [['leds', 'full_foam'], 'string'],
            [['date_completed'], 'safe'],
            [['serial_no', 'stereo', 'special', 'frame', 'jets', 'ext'], 'string', 'max' => 20],
            [['model'], 'string', 'max' => 255],
            [['color', 'skirt'], 'string', 'max' => 100],
            [['csvFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'csv'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'serial_no' => 'Serial No',
            'model' => 'Model',
            'color' => 'Color',
            'skirt' => 'Skirt',
            'leds' => 'Leds',
            'full_foam' => 'Full Foam',
            'stereo' => 'Stereo',
            'special' => 'Special',
            'frame' => 'Frame',
            'jets' => 'Jets',
            'ext' => 'Ext',
            'date_completed' => 'Date Completed',
        ];
    }

    public function upload()
    {
        $dir = 'resources/csv';

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        if($this->csvFile->saveAs($dir.'/csv' . '.' . $this->csvFile->extension)) {
            chmod($dir.'/csv' . '.' . $this->csvFile->extension, 0777);
            return true;
        }

        return false;
    }

    public function uploadData($file) {
        $flag = null;
        $connection = Yii::$app->db;
        $root = getcwd();
        $saveDir = $root.'/resources/csv/csv.csv';
        $sql = "LOAD DATA LOCAL INFILE '$saveDir'
                INTO TABLE inventory
                FIELDS TERMINATED BY ',' ENCLOSED BY '\"' ESCAPED BY '\"'
                LINES TERMINATED BY '\\r\\n'
                IGNORE 1 LINES";

        $transaction =  $connection->beginTransaction();

        try {
            if(!empty($file)){
                $command=$connection->createCommand($sql);
                if($command->execute()){
                    $flag = true;
                    $transaction->commit();
                }
            }
        } catch (Exception $e) {
            $flag = $e->errorInfo[2];
            $transaction->rollBack();
        }

        return $flag;
    }
}
