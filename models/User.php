<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\base\Exception;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $user_type
 * @property string $username
 * @property string $password
 * @property string $customer_no
 * @property string $dealer
 * @property string $email_address
 * @property string $status
 * @property string $ins_time
 * @property string $up_time
 *
 * @property Orders[] $orders
 * @property UserInfo[] $userInfos
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $confirmPassword;
    public $oldPassword;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email_address'], 'required'],
            [['user_type', 'status'], 'string'],
            [['ins_time', 'up_time'], 'safe'],
            [['username'], 'string', 'max' => 10],
            [['password', 'customer_no', 'dealer', 'email_address'], 'string', 'max' => 100],
            [['password', 'confirmPassword', 'customer_no', 'dealer'], 'required', 'on' => 'registerAccount'],
            [['email_address'], 'email'],
            [['email_address', 'username'], 'unique'],
            //[['email_address', 'username', 'customer_no'], 'unique', 'on' => 'registerAccount'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password',
             'message' => Yii::t('app', "Passwords don't match.")],
            [['password', 'confirmPassword', 'oldPassword'], 'required', 'on' => 'changePass'],
            [['password', 'confirmPassword'], 'required', 'on' => 'changePassAdmin'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_type' => 'User Type',
            'username' => 'Username',
            'password' => 'Password',
            'customer_no' => 'Customer No',
            'dealer' => 'Dealer',
            'email_address' => 'Email Address',
            'status' => 'Status',
            'ins_time' => 'Ins Time',
            'up_time' => 'Up Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfos()
    {
        return $this->hasMany(UserInfo::className(), ['user_id' => 'id']);
    }

     /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    /******************************** Identity Interface ********************************/
    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /******************************** Custom Functions ********************************/

    /**
        @created 2016-06-18
        findByUsername()
        @param  [string]    [username]
    **/
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
    *   updateRecord()
    *   Updates the record depending on the ID being passed.
    *
    *   @param    int id - ID of the record to be updated.
    *   @param    array arr - array of column_name => values to be updated.
    */
    public static function updateRecord($id, $arr)
    {
        $record = self::findOne($id);
        foreach ($arr as $key => $val) {
            $record->$key = $val;
        }
        return $record->update(true, array_keys($arr));
    }

    /**
        @created 2016-06-18
        validatePassword()
        @param  [string]    [password] - password inputted by the user
        @param  [string]    [hash_password] - hash password fetch from the database
    **/
    public static function validatePassword($password, $hash_password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $hash_password);
    }

    public function getRecords()
    {
        $query = new Query();

        $query->select(["CONCAT(first_name, ' ', last_name) AS full_name", 'u.*'])
            ->from('user u')
            ->leftJoin('user_info ui', 'u.id = ui.user_id')
            ->where('u.status IN ("active", "unconfirmed") and user_type="user"');

        $command = $query->createCommand(Yii::$app->db);
        $rows = $command->queryAll();

        return $rows;
    }

    public function saveRecord($user, $userInfo, $id=null)
    {
        $connection = Yii::$app->db;
        $transaction =  $connection->beginTransaction();

        try {
            $userModel = (!empty($id)) ? self::findOne($id) : new User;

            if(empty($id)) {
                $userModel->user_type = 'user';
                $userModel->status = 'unconfirmed';
                $userModel->ins_time = Yii::$app->formatter->asDatetime('now');
            }

            $userModel->username = $user['username'];
            $userModel->email_address = $user['email_address'];
            $userModel->customer_no = strtoupper($user['customer_no']);
            $userModel->dealer = $user['dealer'];
            $userModel->up_time = Yii::$app->formatter->asDatetime('now');

            if(!empty($user['password'])) {
                $userModel->password = Yii::$app->getSecurity()->generatePasswordHash($user['password']);
            }

            if($userModel->save()) {
                $userInfoModel = (!empty($id)) ? UserInfo::find()->where(['user_id' => $id])->one() : new UserInfo;
                $userInfoModel->user_id = $userModel->id;
                $userInfoModel->first_name = $userInfo['first_name'];
                $userInfoModel->last_name = $userInfo['last_name'];
                $userInfoModel->status = 'active';

                if($userInfoModel->save()) {
                    $transaction->commit();
                    return true;
                } else {
                    var_dump($userInfoModel->errors); exit;
                    $transaction->rollBack();
                    return false;
                }
            } else {
                var_dump($userModel->errors); exit;
                $transaction->rollBack();
                return false;
            }

        }  catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    public function changePassword($id, $password)
    {
        $record = self::findOne($id);

        $record->password = Yii::$app->getSecurity()->generatePasswordHash($password);

        return $record->save();
    }
}
