<?php

namespace app\assets;

use yii\web\AssetBundle;

class DatatablesAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/DataTables-1.10.15/media/css/dataTables.bootstrap.min.css',
    ];
    public $js = [
        'plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js',
        'plugins/DataTables-1.10.15/media/js/dataTables.bootstrap.min.js',
        'js/datatables.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
