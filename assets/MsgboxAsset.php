<?php

namespace app\assets;

use yii\web\AssetBundle;

class MsgboxAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/bootstrap-sweetalert-master/dist/sweetalert.css',
    ];
    public $js = [
        'plugins/bootstrap-sweetalert-master/dist/sweetalert.js',
        'js/msgbox.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
